;; gnuplot mode from:
;;      http://cars9.uchicago.edu/~ravel/software/gnuplot-mode.html
;;

(when (g-load-path "gnuplot-mode")

  (autoload 'gnuplot-mode "gnuplot" "gnuplot major mode" t)
  (autoload 'gnuplot-make-buffer "gnuplot" "open a buffer in gnuplot mode" t)

  (add-to-list 'auto-mode-alist '("\\.gp\\'" . gnuplot-mode))
  (add-to-list 'auto-mode-alist '("\\.gnuplot\\'" . gnuplot-mode))
)

(provide 'g-gnuplot)
