

(when (require 'ergoemacs-mode nil t)

  (setq ergoemacs-use-menus nil)   
  (setq ergoemacs-theme nil)
  (ergoemacs-mode 1)

  ;;(setq ergoemacs-theme "standard")
    
  (global-set-key (kbd "C-c c") 'helm-for-files)
  (global-set-key (kbd "C-x C-f") 'helm-find-files)
  ;; (global-set-key (kbd "C-x C-f") 'ido-find-files)
  ;; (global-set-key (kbd "C-x b") 'helm-buffers-list)
  ;; (define-key [remap eshell-pcomplete] nil)
  
  (global-set-key (kbd "<f10>") 'dired-jump)
  (global-set-key (kbd "<f9>") 'g-goto-eshell)
  (global-set-key (kbd "<f8>") 'magit-status)
  
  (global-set-key (kbd "M-0") 'ispell-word)
  (global-set-key (kbd "<M-backspace>") 'backward-kill-word)

  ;; visible bookmarks (bm) 
  (global-set-key (kbd "<C-f5>") 'bm-toggle)
  (global-set-key (kbd "<f5>")   'bm-next)
  (global-set-key (kbd "<S-f5>") 'bm-previous)

  
  (define-key dired-mode-map (kbd "c") 'g-rename-file-with-timestamp)



  ;;(ergoemacs-theme-option-on '(guille))




  ;;     (ergoemacs-key "M-0" 'ispell-word "Ispell word" t)
  ;;     (ergoemacs-key "M-a" 'helm-M-x)
  ;;     ;; (ergoemacs-key "C-o" 'helm-swoop)
      
  ;;     (ergoemacs-minor-key 'dired-mode-hook 
  ;;                          '("c" g-rename-file-with-timestamp dired-mode-map))
  ;;     )

  (setq ergoemacs-handle-ctl-c-or-ctl-x (quote only-C-c-and-C-x))
  (setq ergoemacs-use-ergoemacs-key-descriptions nil)
  ;; (setq ergoemacs-theme "gergo")        

  ;; enable smart paste
  ;; (setq ergoemacs-smart-paste t)
  
  ;; disable ido-style return in helm
  (setq ergoemacs-helm-ido-style-return nil)
  
  )

(provide 'g-zergoemacs)
