(when (require 'keyfreq nil t)
  (keyfreq-mode 1)
  (keyfreq-autosave-mode 1))

(provide 'g-keyfreq)
