;; BBDB

(when (require 'bbdb nil t)
  
  (setq bbdb-file (expand-file-name "misc/bbdb.el" g-emacs-dir))

  (bbdb-initialize 'gnus 'message)
  (bbdb-mua-auto-update-init 'gnus 'message)

  (setq bbdb-complete-mail-allow-cycling t)
  (setq bbdb-message-all-addresses t)

  ;; this allows bbdb-mua-edit-field-sender to create new records, 
  (setq bbdb-mua-update-interactive-p '(query . query))

  (setq bbdb-layout 'one-line)
  (setq bbdb-pop-up-layout 'one-line)
  ;; (setq bbdb-nua-pop-up 'horiz)

  ;; size of pop-up window
  ;; (setq bbdb-pop-up-window-size 2)
  (setq bbdb-mua-pop-up-window-size 1)
  ;; (setq bbdb-horiz-pop-up-window-size '(112 .1))
)

(provide 'g-bbdb)
