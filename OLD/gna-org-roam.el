;; org-roam: https://www.orgroam.com/

(defun gna-to-tags-list (comalist)
  (concat ":" (subst-char-in-string ?, ?: comalist) ":"))


(use-package org-roam
  :ensure t
  :after org
  :custom
  (org-roam-directory (expand-file-name "~/Documents/org/org-roam"))
  (orb-preformat-keywords
   '("citekey" "author" "year" "date" "file" "keywords"))
  (org-roam-capture-templates
   '(("d" "default" plain "%?" :if-new
      (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)
     ("r" "bibliography reference" plain
      "
- year :: %^{year}
- auth :: %^{author}
- tags :: %^{keywords}
- file :: [[file:%^{file}][file-link]]

%?"
      :if-new
      (file+head "bib/${citekey}.org"
                 "#+title: ${title}\n#+filetags: :bib:${year}:")
      :unnarrowed t)))
  
  :config 
  (org-roam-setup)
  (require 'org-roam-protocol)
  
  (add-to-list 'display-buffer-alist
               '("\\*org-roam\\*"
                 (display-buffer-in-direction)
                 (direction . right)
                 (window-width . 0.33)
                 (window-height . fit-window-to-buffer))))


;; https://github.com/org-roam/org-roam-ui
(use-package org-roam-ui
  :ensure t
  :after org-roam
  :custom
  (org-roam-ui-browser-function #'browse-url-firefox))


;; https://github.com/ThomasFKJorna/org-roam-timestamps
(use-package org-roam-timestamps
  :ensure t
  :diminish
  :after org-roam
  :config (org-roam-timestamps-mode))


(use-package citar-org-roam
  :after citar org-roam
  :diminish
  :custom
  (citar-org-roam-subdir "bib")
  :config
  (citar-org-roam-mode))


(use-package org-roam-bibtex
  :ensure t
  :after org-roam
  :diminish org-roam-bibtex-mode
  :custom
  (orb-roam-ref-format 'org-cite)
  ;; (orb-autokey-format "A[2][10][.].%y:%T*[10][20][-]")
  (orb-autokey-format "%e{(bibtex-generate-autokey)}")
  :config
  (org-roam-bibtex-mode 1))


(provide 'gna-org-roam)
