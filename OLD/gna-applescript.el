;; applescript stuff
;; guille
;;
;; most of it inspired by jao's

(jao-if-darwin

;; applescript-mode
 (autoload 'applescript-mode "applescript-mode" 
   "major mode for editing AppleScript source." t)
 (setq auto-mode-alist
      (cons '("\\.applescript$" . applescript-mode) auto-mode-alist))
   

 (defun do-applescript (script)
   (with-temp-buffer
     (insert script)
     (shell-command-on-region (point-min) (point-max) "osascript" t)
     (buffer-string)))
 
 (defun g-as-tell-app (app something &optional activate)
   (let ((res (do-applescript 
               (concat "try \n 
                          tell application \"" app "\"\n" 
                          (if activate "activate\n")
                            something "\n"
                          "end tell\n"
                       "on error \n return null \n end try"))))
     (unless activate
       (if (or (string= res "null") (string= res "")) nil
         (substring res 0 -1)))))

 (defun g-as-safari-doc ()
   (interactive)
   (let ((url (g-as-tell-app "Safari" "get the URL of document 1"))
         (name (g-as-tell-app "Safari" "get the name of window 1")))
     (if url (cons url name) nil)))
 
 (defun get-as-conkeror-doc ()
   (interactive)
   (let ((name (g-as-tell-app "Conkeror" "get the name of window 1"))
         (url "n/a"))
     (cons url name)))

 (defun g-as-firefox-doc ()
   (interactive)
   (let ((url (shell-command-to-string
               (concat "osascript "
                       (expand-file-name "misc/firefox-url.applescript" 
                                         g-emacs-dir))))
         (name (g-as-tell-app "System Events"
                              (concat "tell process \"Firefox\" \n"
                                      "get name of windows as string\n"
                                      "end tell\n"))))
         (if name (cons (substring url 0 -1) name) nil)))
)
 
(provide 'g-applescript)
