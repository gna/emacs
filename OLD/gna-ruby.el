;; Ruby stuff
;; (Guille)

;; load ri-emacs subdir
(g-load-path "ruby-mode")
(g-load-path "ri-emacs")

(autoload 'ruby-mode "ruby-mode"
  "Mode for editing ruby source files" t)
(setq auto-mode-alist
      (append '(("\\.rb$" . ruby-mode)) auto-mode-alist))
(setq interpreter-mode-alist (append '(("ruby" . ruby-mode))
                                     interpreter-mode-alist))

(autoload 'run-ruby "inf-ruby"
  "Run an inferior Ruby process")
(autoload 'inf-ruby-keys "inf-ruby"
  "Set local key defs for inf-ruby in ruby-mode")
(add-hook 'ruby-mode-hook
          '(lambda () (inf-ruby-keys)))

   


;; ri-emacs
(setq ri-ruby-script (expand-file-name "ri-emacs/ri-emacs.rb" local-lisp-dir))
(autoload 'ri "ri-ruby.el" nil t)

;; redefine keys for ri
;; (add-hook 'ruby-mode-hook
;;           (lambda ()
;;             ;; (local-set-key (kbd "<f1>")  'ri)
;;             (local-set-key (kbd "M-C-i") 'ri-ruby-complete-symbol)
;;             (local-set-key (kbd "<f4>")  'ri-ruby-show-args)))

(autoload 'run-ruby "Run an inferior Ruby process")
(autoload 'inf-ruby-keys "inf-ruby"
  "Set local keys defs for inf-ruby in ruby-mode")

(add-hook 'ruby-mode-hook '(lambda () (inf-ruby-keys)))

;; ruby-electric
;; (when (require 'ruby-electric nil t)
;;   (add-hook 'ruby-mode-hook '(lambda () (ruby-electric-mode))))

;; rake
(add-to-list 'auto-mode-alist '("\\Rakefile\\'" . ruby-mode))

;; rails
;; (require 'rails)
;; (require 'rhtml-mode)

;; ;; haml and sass
;; (require 'haml-mode nil 't)
;; (require 'sass-mode nil 't)


(provide 'g-ruby)
