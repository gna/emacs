;; dashboard: https://github.com/emacs-dashboard/emacs-dashboard
(use-package dashboard
  :ensure t
  :custom
  ;; (initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
  (dashboard-items '((recents . 12)
                     (projects . 12)
                     (bookmarks . 12)))
  (dashboard-set-footer nil)
  (dashboard-startup-banner nil)
  :config
  (dashboard-setup-startup-hook))


(provide 'gna-dashboard)
