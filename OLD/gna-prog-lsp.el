

(use-package lsp-mode
  :ensure t
  :hook ((lsp-mode . lsp-enable-which-key-integration))
  :commands (lsp lsp-deferred)
  :custom
  (lsp-diagnostics-provider :flymake)
  (lsp-signature-render-documentation nil)
  ;; disable company completion
  (lsp-completion-provider :none)

  :config
  ;; increase Emacs cons-threshold and process-output
  ;; https://emacs-lsp.github.io/lsp-mode/page/performance/
  (setq gc-cons-threshold 100000000)
  (setq read-process-output-max (* 1024 1024)))


(use-package lsp-ui
  :ensure 
  :after lsp-mode
  :bind (:map lsp-ui-mode-map
              ("C-c u" . lsp-ui-imenu)
              ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
              ([remap xref-find-references] . lsp-ui-peek-find-references))
  :commands lsp-ui-mode
  :custom
  (lsp-ui-doc-show-with-cursor nil)
  (lsp-ui-doc-header t))


(use-package lsp-ltex
  :after lsp
  :ensure t
  :custom
  (lsp-ltex-version "15.2.0")
  (lsp-ltex-mother-tongue "es"))


(provide 'gna-prog-lsp)
