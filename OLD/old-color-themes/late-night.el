(eval-when-compile    (require 'color-theme))
(defun color-theme-jao-late-night ()
  "Color theme by Jose Ortega, created 2008-01-09."
  (interactive)
  (color-theme-install
   '(color-theme-jao-late-night
     ((background-color . "#000")
      (background-mode . dark)
      (background-toolbar-color . "#000")
      (border-color . "#000")
      (bottom-toolbar-shadow-color . "#000")
      (cursor-color . "#888")
      (foreground-color . "#666")
      (top-toolbar-shadow-color . "#111"))
     ((compilation-message-face . underline)
      (gnus-article-button-face . gnus-button)
      (gnus-article-mouse-face . highlight)
      (gnus-mouse-face . highlight)
      (gnus-signature-face . gnus-signature)
      (gnus-summary-selected-face . gnus-summary-selected)
      (list-matching-lines-buffer-name-face . underline)
      (list-matching-lines-face . match)
      (rmail-highlight-face . rmail-highlight)
      (tags-tag-face . default)
      (term-default-bg-color . "unspecified-bg")
      (term-default-fg-color . "unspecified-fg")
      (view-highlight-face . highlight)
      (w3m-form-mouse-face . highlight)
      (widget-mouse-face . highlight))
     (default ((t (:stipple nil :background "#000" :foreground "#666" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 1 :width normal :family "default"))))
     (bg:erc-color-face0 ((t (nil))))
     (bg:erc-color-face1 ((t (nil))))
     (bg:erc-color-face10 ((t (nil))))
     (bg:erc-color-face11 ((t (nil))))
     (bg:erc-color-face12 ((t (nil))))
     (bg:erc-color-face13 ((t (nil))))
     (bg:erc-color-face14 ((t (nil))))
     (bg:erc-color-face15 ((t (nil))))
     (bg:erc-color-face2 ((t (nil))))
     (bg:erc-color-face3 ((t (nil))))
     (bg:erc-color-face4 ((t (nil))))
     (bg:erc-color-face5 ((t (nil))))
     (bg:erc-color-face6 ((t (nil))))
     (bg:erc-color-face7 ((t (nil))))
     (bg:erc-color-face8 ((t (nil))))
     (bg:erc-color-face9 ((t (nil))))
     (bold ((t (:bold t :weight bold))))
     (bold-italic ((t (:italic t :bold t :slant italic :weight bold))))
     (border ((t (:background "#000"))))
     (buffer-menu-buffer ((t (:bold t :weight bold))))
     (button ((t (:bold t :weight bold))))
     (c-nonbreakable-space-face ((t (:bold t :background "red" :foreground "black" :weight bold))))
     (calendar-today ((t (:underline t))))
     (comint-highlight-input ((t (:bold t :weight bold))))
     (comint-highlight-prompt ((t (:foreground "cyan1"))))
     (compilation-column-number ((t (:bold t :weight bold))))
     (compilation-error ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (compilation-info ((t (:bold t :foreground "Green1" :weight bold))))
     (compilation-line-number ((t (:bold t :weight bold :foreground "#888"))))
     (compilation-warning ((t (:bold t :foreground "Orange" :weight bold))))
     (completions-common-part ((t (:family "default" :width normal :weight normal :slant normal :underline nil :overline nil :strike-through nil :box nil :inverse-video nil :foreground "#666" :background "#000" :stipple nil :height 1))))
     (completions-first-difference ((t (:bold t :weight bold))))
     (cursor ((t (:background "#888"))))
     (custom-button ((t (:bold t :foreground "#999" :weight bold))))
     (custom-button-mouse ((t (nil))))
     (custom-button-pressed ((t (nil))))
     (custom-button-pressed-unraised ((t (:bold t :weight bold :foreground "violet"))))
     (custom-button-unraised ((t (:bold t :weight bold))))
     (custom-changed ((t (:background "blue1" :foreground "white"))))
     (custom-comment ((t (:background "yellow3" :foreground "black"))))
     (custom-comment-tag ((t (:foreground "gray80"))))
     (custom-documentation ((t (nil))))
     (custom-face-tag ((t (:bold t :weight bold :height 1.2))))
     (custom-group-tag ((t (:bold t :foreground "light blue" :weight bold :height 1.2))))
     (custom-group-tag-1 ((t (:bold t :foreground "pink" :weight bold :height 1.2))))
     (custom-invalid ((t (:background "red1" :foreground "yellow1"))))
     (custom-link ((t (:underline t :foreground "cyan1"))))
     (custom-modified ((t (:background "blue1" :foreground "white"))))
     (custom-rogue ((t (:background "black" :foreground "pink"))))
     (custom-saved ((t (:underline t))))
     (custom-set ((t (:background "white" :foreground "blue1"))))
     (custom-state ((t (:foreground "lime green"))))
     (custom-themed ((t (:background "blue1" :foreground "white"))))
     (custom-variable-button ((t (:bold t :underline t :weight bold))))
     (custom-variable-tag ((t (:bold t :foreground "light blue" :weight bold))))
     (custom-visibility ((t (:underline t :foreground "cyan1" :height 0.8))))
     (diary ((t (:foreground "yellow1"))))
     (dired-directory ((t (:bold t :weight bold :foreground "#777"))))
     (dired-flagged ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (dired-header ((t (:bold t :weight bold))))
     (dired-ignored ((t (:foreground "grey70"))))
     (dired-mark ((t (:foreground "#777"))))
     (dired-marked ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (dired-symlink ((t (:foreground "#777"))))
     (dired-warn-writable ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (dired-warning ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (ediff-current-diff-A ((t (:background "pale green" :foreground "firebrick"))))
     (ediff-current-diff-Ancestor ((t (:background "VioletRed" :foreground "Black"))))
     (ediff-current-diff-B ((t (:background "Yellow" :foreground "DarkOrchid"))))
     (ediff-current-diff-C ((t (:background "Pink" :foreground "Navy"))))
     (ediff-even-diff-A ((t (:background "light grey" :foreground "Black"))))
     (ediff-even-diff-Ancestor ((t (:background "Grey" :foreground "White"))))
     (ediff-even-diff-B ((t (:background "Grey" :foreground "White"))))
     (ediff-even-diff-C ((t (:background "light grey" :foreground "Black"))))
     (ediff-fine-diff-A ((t (:background "sky blue" :foreground "Navy"))))
     (ediff-fine-diff-Ancestor ((t (:background "Green" :foreground "Black"))))
     (ediff-fine-diff-B ((t (:background "cyan" :foreground "Black"))))
     (ediff-fine-diff-C ((t (:background "Turquoise" :foreground "Black"))))
     (ediff-odd-diff-A ((t (:background "Grey" :foreground "White"))))
     (ediff-odd-diff-Ancestor ((t (:background "gray40" :foreground "cyan3"))))
     (ediff-odd-diff-B ((t (:background "light grey" :foreground "Black"))))
     (ediff-odd-diff-C ((t (:background "Grey" :foreground "White"))))
     (eldoc-highlight-function-argument ((t (:bold t :weight bold))))
     (emms-browser-album-face ((t (:foreground "#aaaaff" :height 1.1))))
     (emms-browser-artist-face ((t (:foreground "#aaaaff" :height 1.3))))
     (emms-browser-track-face ((t (:foreground "#aaaaff" :height 1.0))))
     (emms-browser-year/genre-face ((t (:foreground "#aaaaff" :height 1.5))))
     (emms-playlist-selected-face ((t (:foreground "SteelBlue3"))))
     (emms-playlist-track-face ((t (:foreground "DarkSeaGreen"))))
     (emms-stream-name-face ((t (:bold t :weight bold))))
     (emms-stream-url-face ((t (:foreground "LightSteelBlue"))))
     (erc-action-face ((t (nil))))
     (erc-bold-face ((t (:bold t :weight bold))))
     (erc-current-nick-face ((t (:bold t :weight bold))))
     (erc-default-face ((t (nil))))
     (erc-direct-msg-face ((t (nil))))
     (erc-error-face ((t (:bold t :foreground "IndianRed" :weight bold))))
     (erc-highlight-face ((t (:bold t :foreground "pale green" :weight bold))))
     (erc-input-face ((t (:foreground "#555"))))
     (erc-inverse-face ((t (:background "steel blue"))))
     (erc-keyword-face ((t (:bold t :foreground "#999" :weight bold))))
     (erc-nick-msg-face ((t (:foreground "#888"))))
     (erc-notice-face ((t (:foreground "#444"))))
     (erc-pal-face ((t (:foreground "#888"))))
     (erc-prompt-face ((t (:bold t :foreground "#777" :weight bold))))
     (erc-timestamp-face ((t (:bold t :foreground "#777" :weight bold))))
     (escape-glyph ((t (:foreground "cyan"))))
     (eshell-ls-archive ((t (:bold t :foreground "Orchid" :weight bold))))
     (eshell-ls-backup ((t (:foreground "LightSalmon"))))
     (eshell-ls-clutter ((t (:bold t :foreground "OrangeRed" :weight bold))))
     (eshell-ls-directory ((t (:bold t :foreground "SkyBlue" :weight bold))))
     (eshell-ls-executable ((t (:bold t :foreground "Green" :weight bold))))
     (eshell-ls-missing ((t (:bold t :foreground "Red" :weight bold))))
     (eshell-ls-product ((t (:foreground "LightSalmon"))))
     (eshell-ls-readonly ((t (:foreground "Pink"))))
     (eshell-ls-special ((t (:bold t :foreground "Magenta" :weight bold))))
     (eshell-ls-symlink ((t (:bold t :foreground "Cyan" :weight bold))))
     (eshell-ls-unreadable ((t (:foreground "DarkGrey"))))
     (eshell-prompt ((t (:bold t :foreground "Pink" :weight bold))))
     (ffap ((t (:foreground "light blue" :background "dark slate blue"))))
     (fg:erc-color-face0 ((t (:foreground "white"))))
     (fg:erc-color-face1 ((t (:foreground "beige"))))
     (fg:erc-color-face10 ((t (:foreground "pale goldenrod"))))
     (fg:erc-color-face11 ((t (:foreground "light goldenrod yellow"))))
     (fg:erc-color-face12 ((t (:foreground "light yellow"))))
     (fg:erc-color-face13 ((t (:foreground "yellow"))))
     (fg:erc-color-face14 ((t (:foreground "light goldenrod"))))
     (fg:erc-color-face15 ((t (:foreground "lime green"))))
     (fg:erc-color-face2 ((t (:foreground "lemon chiffon"))))
     (fg:erc-color-face3 ((t (:foreground "light cyan"))))
     (fg:erc-color-face4 ((t (:foreground "powder blue"))))
     (fg:erc-color-face5 ((t (:foreground "sky blue"))))
     (fg:erc-color-face6 ((t (:foreground "dark sea green"))))
     (fg:erc-color-face7 ((t (:foreground "pale green"))))
     (fg:erc-color-face8 ((t (:foreground "medium spring green"))))
     (fg:erc-color-face9 ((t (:foreground "khaki"))))
     (file-name-shadow ((t (:foreground "grey70"))))
     (fixed-pitch ((t (:family "courier"))))
     (font-lock-builtin-face ((t (:bold t :foreground "#777" :weight bold))))
     (font-lock-comment-delimiter-face ((t (:foreground "#555"))))
     (font-lock-comment-face ((t (:foreground "#555"))))
     (font-lock-constant-face ((t (:foreground "#777"))))
     (font-lock-doc-face ((t (:foreground "#777"))))
     (font-lock-doc-string-face ((t (:foreground "#777"))))
     (font-lock-function-name-face ((t (:bold t :foreground "#777" :weight bold))))
     (font-lock-keyword-face ((t (:foreground "#777"))))
     (font-lock-negation-char-face ((t (nil))))
     (font-lock-preprocessor-face ((t (:foreground "#777"))))
     (font-lock-reference-face ((t (:foreground "#777"))))
     (font-lock-regexp-grouping-backslash ((t (:bold t :weight bold))))
     (font-lock-regexp-grouping-construct ((t (:bold t :weight bold))))
     (font-lock-string-face ((t (:foreground "#777"))))
     (font-lock-type-face ((t (:bold t :weight bold))))
     (font-lock-variable-name-face ((t (:bold t :foreground "#888" :weight bold))))
     (font-lock-warning-face ((t (:bold t :background "black" :foreground "red" :weight bold))))
     (fringe ((t (:background "#111" :foreground "#444"))))
     (gnus-button ((t (:bold t :weight bold))))
     (gnus-cite-attribution-face ((t (:foreground "#bbb"))))
     (gnus-cite-face-1 ((t (:foreground "#aaa"))))
     (gnus-cite-face-2 ((t (:foreground "#aaa"))))
     (gnus-cite-face-3 ((t (:foreground "#aaa"))))
     (gnus-cite-face-4 ((t (:foreground "#aaa"))))
     (gnus-cite-face-5 ((t (:foreground "#aaa"))))
     (gnus-cite-face-6 ((t (:foreground "#aaa"))))
     (gnus-cite-face-7 ((t (:foreground "#aaa"))))
     (gnus-cite-face-8 ((t (:foreground "#aaa"))))
     (gnus-cite-face-9 ((t (:foreground "#aaa"))))
     (gnus-emphasis-bold ((t (:bold t :weight bold))))
     (gnus-emphasis-bold-italic ((t (:italic t :bold t :slant italic :weight bold))))
     (gnus-emphasis-highlight-words ((t (:foreground "#ccc"))))
     (gnus-emphasis-italic ((t (:italic t :slant italic))))
     (gnus-emphasis-strikethru ((t (:strike-through t))))
     (gnus-emphasis-underline ((t (:underline t))))
     (gnus-emphasis-underline-bold ((t (:bold t :underline t :weight bold))))
     (gnus-emphasis-underline-bold-italic ((t (:italic t :bold t :underline t :slant italic :weight bold))))
     (gnus-emphasis-underline-italic ((t (:italic t :underline t :slant italic))))
     (gnus-group-mail-1 ((t (:bold t :foreground "#999" :weight bold))))
     (gnus-group-mail-1-empty ((t (:foreground "#999"))))
     (gnus-group-mail-2 ((t (:bold t :foreground "#999" :weight bold))))
     (gnus-group-mail-2-empty ((t (:foreground "#999"))))
     (gnus-group-mail-3 ((t (:bold t :foreground "#888" :weight bold))))
     (gnus-group-mail-3-empty ((t (:foreground "#888"))))
     (gnus-group-mail-low ((t (:bold t :foreground "#777" :weight bold))))
     (gnus-group-mail-low-empty ((t (:foreground "#777"))))
     (gnus-group-news-1 ((t (:bold t :foreground "#999" :weight bold))))
     (gnus-group-news-1-empty ((t (:foreground "#999"))))
     (gnus-group-news-2 ((t (:bold t :foreground "#888" :weight bold))))
     (gnus-group-news-2-empty ((t (:foreground "#888"))))
     (gnus-group-news-3 ((t (:bold t :foreground "#777" :weight bold))))
     (gnus-group-news-3-empty ((t (:foreground "#777"))))
     (gnus-group-news-4 ((t (:bold t :foreground "#666" :weight bold))))
     (gnus-group-news-4-empty ((t (:foreground "#666"))))
     (gnus-group-news-5 ((t (:bold t :foreground "#666" :weight bold))))
     (gnus-group-news-5-empty ((t (:foreground "#666"))))
     (gnus-group-news-6 ((t (:bold t :foreground "#666" :weight bold))))
     (gnus-group-news-6-empty ((t (:foreground "#666"))))
     (gnus-group-news-low ((t (:bold t :foreground "#666" :weight bold))))
     (gnus-group-news-low-empty ((t (:foreground "#666"))))
     (gnus-header-content ((t (:italic t :foreground "#888" :slant italic))))
     (gnus-header-from ((t (:bold t :foreground "#888" :weight bold))))
     (gnus-header-name ((t (:bold t :foreground "#777" :weight bold))))
     (gnus-header-newsgroups ((t (:italic t :bold t :foreground "#777" :slant italic :weight bold))))
     (gnus-header-subject ((t (:bold t :foreground "#999" :weight bold))))
     (gnus-server-agent ((t (:bold t :foreground "PaleTurquoise" :weight bold))))
     (gnus-server-closed ((t (:italic t :foreground "LightBlue" :slant italic))))
     (gnus-server-denied ((t (:bold t :foreground "Pink" :weight bold))))
     (gnus-server-offline ((t (:bold t :foreground "Yellow" :weight bold))))
     (gnus-server-opened ((t (:bold t :foreground "Green1" :weight bold))))
     (gnus-signature ((t (:italic t :foreground "#444" :slant italic))))
     (gnus-splash ((t (:foreground "#ccc"))))
     (gnus-summary-cancelled ((t (:background "#555" :foreground "#000"))))
     (gnus-summary-high-ancient ((t (:bold t :foreground "#555" :weight bold))))
     (gnus-summary-high-read ((t (:bold t :foreground "#666" :weight bold))))
     (gnus-summary-high-ticked ((t (:bold t :foreground "#777" :weight bold))))
     (gnus-summary-high-undownloaded ((t (:bold t :foreground "LightGray" :weight bold))))
     (gnus-summary-high-unread ((t (:bold t :foreground "#888" :weight bold))))
     (gnus-summary-low-ancient ((t (:italic t :foreground "#444" :slant italic))))
     (gnus-summary-low-read ((t (:italic t :foreground "#555" :slant italic))))
     (gnus-summary-low-ticked ((t (:italic t :foreground "#666" :slant italic))))
     (gnus-summary-low-undownloaded ((t (:italic t :foreground "LightGray" :slant italic :weight normal))))
     (gnus-summary-low-unread ((t (:italic t :foreground "#777" :slant italic))))
     (gnus-summary-normal-ancient ((t (:foreground "#555"))))
     (gnus-summary-normal-read ((t (:foreground "#666"))))
     (gnus-summary-normal-ticked ((t (:foreground "#777"))))
     (gnus-summary-normal-undownloaded ((t (:foreground "LightGray" :weight normal))))
     (gnus-summary-normal-unread ((t (:foreground "#888"))))
     (gnus-summary-selected ((t (:background "#333" :underline t))))
     (header-line ((t (:background "#333" :foreground "#000"))))
     (help-argument-name ((t (nil))))
     (highlight ((t (:background "dark slate blue" :foreground "light blue"))))
     (holiday ((t (:background "#000" :foreground "#777"))))
     (info-header-node ((t (:foreground "#666"))))
     (info-header-xref ((t (:foreground "#666"))))
     (info-menu-header ((t (:bold t :foreground "#666" :weight bold))))
     (info-menu-star ((t (:foreground "red1" :underline t))))
     (info-node ((t (:bold t :foreground "#888" :weight bold))))
     (info-title-1 ((t (:bold t :foreground "yellow" :weight bold))))
     (info-title-2 ((t (:bold t :foreground "lightblue" :weight bold))))
     (info-title-3 ((t (:bold t :weight bold))))
     (info-title-4 ((t (:bold t :weight bold))))
     (info-xref ((t (:bold t :foreground "#777" :weight bold))))
     (info-xref-visited ((t (:foreground "violet" :underline t))))
     (isearch ((t (:background "red" :foreground "pink"))))
     (iswitchb-current-match ((t (:bold t :weight bold :foreground "#777"))))
     (iswitchb-invalid-regexp ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (iswitchb-single-match ((t (:foreground "#555"))))
     (iswitchb-virtual-matches ((t (:bold t :weight bold :foreground "#777"))))
     (italic ((t (:bold t :weight bold))))
     (lazy-highlight ((t (:background "paleturquoise4" :foreground "red"))))
     (link ((t (:foreground "cyan1" :underline t))))
     (link-visited ((t (:underline t :foreground "violet"))))
     (match ((t (:background "RoyalBlue3"))))
     (menu ((t (:background "#111" :foreground "#444"))))
     (message-cited-text ((t (:foreground "#aaa"))))
     (message-header-cc ((t (:bold t :foreground "#888" :weight bold))))
     (message-header-name ((t (:bold t :foreground "#777" :weight bold))))
     (message-header-newsgroups ((t (:italic t :bold t :foreground "#777" :slant italic :weight bold))))
     (message-header-other ((t (:foreground "#666"))))
     (message-header-subject ((t (:bold t :foreground "#999" :weight bold))))
     (message-header-to ((t (:bold t :foreground "#777" :weight bold))))
     (message-header-xheader ((t (:foreground "#666"))))
     (message-mml ((t (:foreground "MediumSpringGreen"))))
     (message-separator ((t (:foreground "#999"))))
     (minibuffer-prompt ((t (:foreground "555"))))
     (mm-uu-extract ((t (:background "dark green" :foreground "light yellow"))))
     (mm-uu-extract ((t (:background "navy" :foreground "white"))))
     (mode-line ((t (:background "gray20" :foreground "grey60" :bold t))))
     (mode-line-buffer-id ((t (nil))))
     (mode-line-inactive ((t (:background "gray20" :foreground "gray30"))))
     (mouse ((t (nil))))
     (muse-bad-link ((t (:bold t :foreground "coral" :underline "coral" :weight bold))))
     (muse-emphasis-1 ((t (:italic t :slant italic))))
     (muse-emphasis-2 ((t (:bold t :weight bold))))
     (muse-emphasis-3 ((t (:italic t :bold t :slant italic :weight bold))))
     (muse-header-1 ((t (:bold t :foreground "#777" :weight bold))))
     (muse-header-2 ((t (:bold t :foreground "#888" :weight bold))))
     (muse-header-3 ((t (:foreground "#777"))))
     (muse-header-4 ((t (:bold t :foreground "#777" :weight bold))))
     (muse-header-5 ((t (:foreground "#555"))))
     (muse-link ((t (:bold t :foreground "cyan" :underline "cyan" :weight bold))))
     (muse-verbatim ((t (:foreground "gray"))))
     (next-error ((t (:foreground "cyan" :background "dark cyan"))))
     (nobreak-space ((t (:foreground "cyan" :underline t))))
     (org-agenda-restriction-lock ((t (:background "skyblue4"))))
     (org-agenda-structure ((t (:foreground "LightSkyBlue"))))
     (org-archived ((t (:foreground "grey70"))))
     (org-code ((t (:foreground "grey70"))))
     (org-column ((t (:background "grey30" :height 1 :family "default"))))
     (org-date ((t (:foreground "Cyan" :underline t))))
     (org-done ((t (:bold t :foreground "PaleGreen" :weight bold))))
     (org-drawer ((t (:foreground "LightSkyBlue"))))
     (org-ellipsis ((t (:foreground "LightGoldenrod" :underline t))))
     (org-formula ((t (:foreground "chocolate1"))))
     (org-headline-done ((t (:foreground "LightSalmon"))))
     (org-hide ((t (:foreground "black"))))
     (org-latex-and-export-specials ((t (:foreground "burlywood"))))
     (org-level-1 ((t (:bold t :foreground "#777" :weight bold))))
     (org-level-2 ((t (:bold t :foreground "#888" :weight bold))))
     (org-level-3 ((t (:foreground "#777"))))
     (org-level-4 ((t (:foreground "#555"))))
     (org-level-5 ((t (:bold t :weight bold))))
     (org-level-6 ((t (:foreground "#777"))))
     (org-level-7 ((t (:bold t :foreground "#777" :weight bold))))
     (org-level-8 ((t (:foreground "#777"))))
     (org-link ((t (:foreground "Cyan" :underline t))))
     (org-property-value ((t (nil))))
     (org-scheduled-previously ((t (:foreground "chocolate1"))))
     (org-scheduled-today ((t (:foreground "PaleGreen"))))
     (org-sexp-date ((t (:foreground "Cyan"))))
     (org-special-keyword ((t (:foreground "LightSalmon"))))
     (org-table ((t (:foreground "LightSkyBlue"))))
     (org-tag ((t (:bold t :weight bold))))
     (org-target ((t (:underline t))))
     (org-time-grid ((t (:foreground "LightGoldenrod"))))
     (org-todo ((t (:bold t :foreground "Pink" :weight bold))))
     (org-upcoming-deadline ((t (:foreground "chocolate1"))))
     (org-verbatim ((t (:foreground "grey70" :underline t))))
     (org-warning ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (outline-1 ((t (:bold t :weight bold :foreground "#777"))))
     (outline-2 ((t (:bold t :weight bold :foreground "#888"))))
     (outline-3 ((t (:foreground "#777"))))
     (outline-4 ((t (:foreground "#555"))))
     (outline-5 ((t (:bold t :weight bold))))
     (outline-6 ((t (:foreground "#777"))))
     (outline-7 ((t (:bold t :weight bold :foreground "#777"))))
     (outline-8 ((t (:foreground "#777"))))
     (p4-depot-added-face ((t (:foreground "blue"))))
     (p4-depot-branch-op-face ((t (:foreground "blue4"))))
     (p4-depot-deleted-face ((t (:foreground "red"))))
     (p4-depot-unmapped-face ((t (:foreground "grey30"))))
     (p4-diff-change-face ((t (:foreground "dark green"))))
     (p4-diff-del-face ((t (:foreground "red"))))
     (p4-diff-file-face ((t (:background "gray90"))))
     (p4-diff-head-face ((t (:background "gray95"))))
     (p4-diff-ins-face ((t (:foreground "blue"))))
     (py-builtins-face ((t (nil))))
     (py-decorators-face ((t (nil))))
     (py-pseudo-keyword-face ((t (nil))))
     (query-replace ((t (:foreground "pink" :background "red"))))
     (region ((t (:background "dark cyan" :foreground "cyan"))))
     (rs-gnus-face-1 ((t (:stipple nil :background "#000" :foreground "#666" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 1 :width normal :family "default"))))
     (scroll-bar ((t (nil))))
     (secondary-selection ((t (:background "Aquamarine" :foreground "SlateBlue"))))
     (shadow ((t (:foreground "grey70"))))
     (show-paren-match ((t (:background "light slate blue" :foreground "white"))))
     (show-paren-mismatch ((t (:background "red" :foreground "white"))))
     (speedbar-button-face ((t (:foreground "green3"))))
     (speedbar-directory-face ((t (:foreground "light blue"))))
     (speedbar-file-face ((t (:foreground "cyan"))))
     (speedbar-highlight-face ((t (:background "sea green"))))
     (speedbar-selected-face ((t (:foreground "red" :underline t))))
     (speedbar-separator-face ((t (:background "blue" :foreground "white" :overline "gray"))))
     (speedbar-tag-face ((t (:foreground "yellow"))))
     (tool-bar ((t (:background "#111" :foreground "#777"))))
     (tooltip ((t (:background "#333" :foreground "#777"))))
     (trailing-whitespace ((t (:background "red1"))))
     (underline ((t (:bold t :weight bold))))
     (variable-pitch ((t (nil))))
     (vertical-border ((t (:foreground "#444" :background "#000"))))
     (w3m-anchor ((t (:foreground "cyan"))))
     (w3m-arrived-anchor ((t (:foreground "LightSkyBlue"))))
     (w3m-bold ((t (:bold t :weight bold))))
     (w3m-current-anchor ((t (:bold t :underline t :weight bold))))
     (w3m-form ((t (:foreground "red" :underline t))))
     (w3m-form-button ((t (:foreground "red" :underline t))))
     (w3m-form-button-mouse ((t (:foreground "red" :underline t))))
     (w3m-form-button-pressed ((t (:foreground "red" :underline t))))
     (w3m-header-line-location-content ((t (:background "Gray20" :foreground "LightGoldenrod"))))
     (w3m-header-line-location-title ((t (:background "Gray20" :foreground "Cyan"))))
     (w3m-history-current-url ((t (:background "yellow1" :foreground "navy"))))
     (w3m-image ((t (:foreground "PaleGreen"))))
     (w3m-insert ((t (:foreground "orchid"))))
     (w3m-italic ((t (:italic t :slant italic))))
     (w3m-session-select ((t (:foreground "cyan"))))
     (w3m-session-selected ((t (:bold t :foreground "cyan" :underline t :weight bold))))
     (w3m-strike-through ((t (:strike-through t))))
     (w3m-tab-mouse ((t (nil))))
     (w3m-tab-selected
      ((t (:foreground "white" :background "grey30" :underline nil))))
     (w3m-tab-selected-background
      ((t (:background "grey40" :foreground "lightgoldenrod"))))
     (w3m-tab-selected-retrieving
      ((t (:inherit w3m-tab-selected :foreground "khaki"))))
     (w3m-tab-unselected
      ((t (:foreground "gray50" :background "gray20" :underline nil))))
     (w3m-tab-background ((t (:inherit w3m-tab-unselected))))
     (w3m-tab-unselected-retrieving
      ((t (:inherit w3m-tab-unselected :foreground "khaki"))))
     (w3m-underline ((t (:underline t))))
     (widget-button ((t (:bold t :foreground "#888" :weight bold))))
     (widget-button-pressed ((t (:foreground "red1"))))
     (widget-documentation ((t (:foreground "lime green"))))
     (widget-field ((t (:bold t :background "yellow3" :foreground "#999" :weight bold))))
     (widget-inactive ((t (:foreground "grey70"))))
     (widget-single-line-field ((t (:background "green3" :foreground "black")))))))
(add-to-list 'color-themes '(color-theme-jao-late-night  "Late Night" "jao"))