
(use-package ranger :ensure t
  :commands (ranger)
  :bind (("<f12>" . ranger)
         :map ranger-mode-map
         ("C-h" . nil)
         ("C-n" . nil)
         ("C-w" . nil)
         ("C-p" . nil)
         ("C-u" . nil)
         ("C-r" . nil)
         ("C-b" . nil)
         ("C-f" . nil)
         ("C-x M-o" . ranger-toggle-dotfiles)
         ("M-g" . ranger-go)
         ("g" . ranger.refresh)
         ("C-v" . ranger-page-down)
         ("M-v" . ranger-page-up)
         :map dired-mode-map
         ("C-p" . nil)))

(provide g-ranger)
