
(use-package projectile
  :ensure t
  :bind (:map projectile-mode-map
              ("C-c p" . projectile-command-map))
  :custom
  (projectile-indexing-method 'hybrid)
  (projectile-sort-order 'recently-active)
  (projectile-switch-project-action #'projectile-dired)
  (projectile-dynamic-mode-line nil)
  (projectile-mode-line-prefix " .")
  (projectile--mode-line projectile-mode-line-prefix)
  :config
  (projectile-global-mode))

(provide 'gna-projectile)
