;; session stuff

;; Sessions
(require 'saveplace)
(setq-default save-place t)
(setq save-place-file (expand-file-name "~/.emacs.d/places"))
(setq session-save-file (expand-file-name "~/.emacs.d/session"))

(require 'session nil t)
(setq session-initialize t)
(setq session-globals-include
      '((kill-ring 50 t)
        (session-file-alist 100 t)
        (file-name-history 200 t)))

(add-hook 'after-init-hook 'session-initialize)

;;; recentf
(setq recentf-save-file (expand-file-name "~/.emacs.d/recentf"))
(require 'recentf)
(recentf-mode 1)

(defun recentf-ido-find-file ()
  "Use ido to select a recently opened file from the `recentf-list'"
  (interactive)
  (let ((home (expand-file-name (getenv "HOME"))))
    (find-file
     (ido-completing-read "Recentf open: "
                          (mapcar (lambda (path)
                                    (replace-regexp-in-string home "~" path))
                                  recentf-list)
                          nil t))))

;;; (global-set-key (kbd "C-c f") 'recentf-ido-find-file)

(provide 'g-session)
