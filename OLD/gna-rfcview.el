; RFC (rfcview-mode)

(setq auto-mode-alist
      (cons '("/rfc[0-9]+\\.txt\\(\\.gz\\)?\\'" . rfcview-mode)
            auto-mode-alist))

(autoload 'rfcview-mode "rfcview" nil t)

(defun rfc (num)
  "Show RFC NUM in a buffer."
  (interactive "nRFC (0 for index): ")
  (let ((url (if (zerop num)
                 "http://www.ietf.org/iesg/1rfc_index.txt"
               (format "http://www.ietf.org/rfc/rfc%i.txt" num)))
        (buf (get-buffer-create "*RFC*")))
    (with-current-buffer buf
      (let ((inhibit-read-only t))
        (delete-region (point-min) (point-max))
        (let ((proc (start-process "wget" buf "wget" "-q" "-O" "-" url)))
          (set-process-sentinel proc 'rfc-sentinel))
        (message "Getting RFC %i..." num)))))

(defun rfc-sentinel (proc event)
  "Sentinel for `rfc'."
  (with-current-buffer (process-buffer proc)
    (goto-char (point-min))
    (view-mode 1)
    (when (fboundp 'rfcview-mode)
      (rfcview-mode)))
  (display-buffer (process-buffer proc)))

(provide 'g-rfcview)
