;; EasyPG config
;; guille



(when (require 'epa-file nil t)
  (epa-file-enable)
  (setq epa-armor t)
  (jao-if-darwin
   (setq epg-gpg-program "gpg")))

(provide 'g-epa)
