
(use-package undo-tree
  :ensure t
  :diminish undo-tree-mode
  :config (global-undo-tree-mode 1)
  :bind ("C-x u" . undo-tree-visualize))


(provide 'gna-undo-tree)
