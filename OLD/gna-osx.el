;; misc OSX stuff


(jao-if-darwin

 ;;key bindings (cmd is meta)
 (setq mac-command-modifier 'meta)
 ;; (option is option)
 (setq mac-option-modifier nil)

 ;; (when (require 'mac-print-mode nil t)
 ;;   (mac-print-mode 1)
 ;;   (global-set-key (kbd "M-p") 'mac-print-buffer))

 ;; never create a new frame when opening files 
 (setq ns-pop-up-frames nil)
 
 ;;  Jao's mdfind
 ;; (require 'mdfind)

 ;; ;; Jao's quicksilver functions
 ;; (defun jao-qs-buffer ()
 ;;   "Opens the current file in Quicksilver"
 ;;   (interactive)
 ;;   (cond ((and buffer-file-name (file-exists-p buffer-file-name))
 ;;          (call-process-shell-command (concat "qs \"" buffer-file-name "\"")))
 ;;         ;; dired handling
 ;;         ((eq major-mode 'dired-mode)
 ;;          (dired-do-shell-command "qs * "
 ;;                                  current-prefix-arg
 ;;                                  (dired-get-marked-files t current-prefix-
 ;;                                                          arg)))
 ;;         ;; buffer-menu mode
 ;;         ((and (eq major-mode 'Buffer-menu-mode)
 ;;               (file-exists-p (buffer-file-name (Buffer-menu-buffer nil))))
 ;;          (call-process-shell-command
 ;;           (concat "qs \"" (buffer-file-name (Buffer-menu-buffer nil)) "\"")))
 ;;         (t
 ;;          (error "Not visiting a file or file doesn't exist"))))

 ;; (defun jao-qs-region (start end)
 ;;   "Opens the contents of the region in Quicksilver as text."
 ;;   (interactive "r")
 ;;   (call-process-region start end "qs" nil 0 nil "-")) )
)
(provide 'g-osx)
