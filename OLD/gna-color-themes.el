;; color themes & co
;; G. Navarro

;;; (when (and (require 'color-theme nil t))
;;;   (color-theme-initialize)
;;;   (let ((g-themes-dir (expand-file-name "color-themes" g-emacs-dir)))
;;;     (mapc 'load-file (directory-files g-themes-dir t ".+\\.el$" t)))
;;;   (if window-system (color-theme-g-clear)
;;;     (color-theme-blues-xterm)))

;;; (when (require 'color-theme nil t)
;;;   (color-theme-initialize)
;;;   (load-file (expand-file-name "color-themes/g-clear.el" g-emacs-dir))
;;;   (color-theme-g-clear)
;;;   (add-hook 'after-make-frame-functions
;;;             (lambda (frame)
;;;               (set-variable 'color-theme-is-global nil)
;;;               (select-frame frame)
;;;               (color-theme-g-clear))))


;;(g-load-path "color-theme")
(when (require 'color-theme nil t)
  (set-variable 'color-theme-is-global t)
  (color-theme-initialize)
  (load-file (expand-file-name "color-themes/g-dark.el" g-emacs-dir))
  (load-file (expand-file-name "color-themes/g-clear.el" g-emacs-dir))
  (color-theme-g-dark))
  

(provide 'g-color-themes)

