
(require 'god-mode)

(global-set-key (kbd "<escape>") 'god-mode-all)

(defun c/god-mode-update-cursor ()
  (cond (god-local-mode 
         (progn (set-face-background 'mode-line "pink2")
                (set-face-background 'fringe "pink2")))
        (t 
         (progn (set-face-background 'mode-line "gray90")
                (set-face-background 'fringe "grey80")))))
(add-hook 'god-mode-enabled-hook 'c/god-mode-update-cursor)
(add-hook 'god-mode-disabled-hook 'c/god-mode-update-cursor)
 
(provide 'g-godmode)
