
(require 'gnus)
(require 'smtpmail)

(setq user-full-name "Guillermo Navarro-Arribas")

;; set gnus as default for handling mail in Emacs
(setq mail-user-agent 'gnus-user-agent)
(setq read-mail-command 'gnus)
(setq send-mail-function 'smtpmail-send-it)
(setq message-send-mail-function 'smtpmail-send-it)


(setq gnus-select-method '(nnnil nil))

;; IMAP
;; Server name: outlook.office365.com
;; Port: 993
;; Encryption method: TLS
(setq gnus-secondary-select-methods
      '((nnimap "uab-mail"
                (nnimap-address "outlook.office365.com")
                (nnimap-server-port "imaps")
                (nnimap-stream ssl))))

(setq gnus-use-cache t)

;; SMTP
;; Server name: smtp.office365.com
;; Port: 587
;; Encryption method: STARTTLS
(setq smtpmail-smtp-server "smtp.office365.com")
(setq smtpmail-smtp-service 587)

;; enable topic mode by default
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

;; set the "from" addres from the "to" when replaying for my addresses
(setq message-alternative-emails
      (regexp-opt '("guillermo.navarro@uab.es"
                    "guillermo.navarro@uab.cat")))

;; don't display smileys
(setq gnus-treat-display-smileys nil)
;; http://groups.google.com/group/gnu.emacs.gnus/browse_thread/thread/a673a74356e7141f
(when window-system
  (setq gnus-sum-thread-tree-indent "  ")
  (setq gnus-sum-thread-tree-root "") ;; "● ")
  (setq gnus-sum-thread-tree-false-root "") ;; "◯ ")
  (setq gnus-sum-thread-tree-single-indent "") ;; "◎ ")
  (setq gnus-sum-thread-tree-vertical        "│")
  (setq gnus-sum-thread-tree-leaf-with-other "├─► ")
  (setq gnus-sum-thread-tree-single-leaf     "╰─► "))

(setq gnus-summary-line-format
      (concat
       "%0{%U%R%z%}"
       "%3{│%}" "%1{%d%}" "%3{│%}" ;; date
       "  "
       "%4{%-20,20f%}"               ;; name
       "  "
       "%3{│%}"
       " "
       "%1{%B%}"
       "%s\n"))
(setq gnus-summary-display-arrow t)

(setq gnus-group-line-format " %m%S%p%P:%~(pad-right 30)G %6y %B\n")
(setq gnus-topic-line-format "%i[ %(%{%n%}%) -- %A ]%v\n")
