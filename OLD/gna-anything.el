;;; g-anything.el --- anything stuff
;;   Guillermo Navarro-Arribas


;; (g-load-path "anything")
(g-load-path "anything/anything-config")

(require 'anything-config)

;; anything for dired
(anything-dired-bindings)

(defun g-anything ()
  (interactive)
  (anything-other-buffer
   '(anything-c-source-recentf
     anything-c-source-buffers+
     anything-c-source-ffap-guesser
     anything-c-source-file-name-history
     anything-c-source-locate
     ;; anything-c-source-tracker-search
     anything-c-source-kill-ring
     ;; anything-c-source-emacs-commands
     ;; anything-c-source-bbdb
     )
   "*g-anything*"))


(global-set-key (kbd "C-c c") 'g-anything)


(provide 'g-anything)
;;; g-anything.el ends here
