;;; g-predictive.el --- guille

(require 'cl)

;;; predictive
(g-load-path "predictive")
(g-load-path "predictive/latex")

(when (require 'predictive nil t)

  (require 'predictive-latex)

  (defconst g-predictive-dict-dir 
    (expand-file-name "misc/predictive" g-emacs-dir) 
    "Directory with predictive dictionaries")
  
  (add-to-list `load-path g-predictive-dict-dir)
  
  ;; auto learn
  (setq predictive-autosave t)
  (setq predictive-auto-learn t)
  (setq predictive-auto-add-to-dict t)
;;   (setq predictive-add-to-dict-ask nil)
  (setq predictive-which-dict t)
  
    (defun g-predictive-british ()
    (interactive)
    (g-predictive-dict "british"))
  
  (defun g-predictive-spanish ()
    (interactive)
    (g-predictive-dict "spanish"))
  
  (defun g-predictive-catalan ()
    (interactive)
    (g-predictive-dict "catalan"))
  
  (defun g-predictive-dict (lang)
    (predictive-mode 1)
    (let ((dict-name (concat "g-" lang)))
      (require (intern dict-name))
      (predictive-set-main-dict dict-name))))

(provide 'g-predictive)
;;; g-predictive.el ends here
