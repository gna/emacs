;; Emacs muse
;; G. Navarro

;; TODO: add stylesheet when publishing in xhtml

(g-load-path "muse/lisp")
(g-load-path "muse/experimental")

(when (require 'muse-mode nil t)

  (require 'muse-project)
  (require 'muse-html)
  (require 'muse-colors)
  (require 'muse-wiki)
  (require 'muse-latex)
  (require 'muse-publish)

  ;; generic publishing dir
  (defvar g-sites-dir (jao-d-l "~/Sites" "~/var/sites"))
  ;; generic misc. dir
  (defvar g-muse-misc-dir (expand-file-name "misc/muse/"
                                            g-emacs-dir))

  (setq muse-project-alist
        `(("website"
           (,(expand-file-name "muse/web" g-docs-dir) :default "web")
            (:base "web-xhtml"
                   :path ,(expand-file-name "web/html" g-sites-dir)))
;;             (:base "web-pdf"
;;                    :path ,(expand-file-name "web/pdf" g-sites-dir)))
          ("sicp-exercises"
           (,(expand-file-name "muse/sicp-exercises" g-docs-dir)
            :default "sicp-exercises")
           (:base "xhtml" :path ,(expand-file-name "sicp-exercises/html"
                                                   g-sites-dir)))
          ("notes"
           (,(expand-file-name "muse/notes" g-docs-dir)
            :default "notes")
           (:base "xhtml" :path ,(expand-file-name "notes/html" g-sites-dir)))
          ("notes"
           (,(expand-file-name "muse/rnotes" g-docs-dir)
            :default "rnotes")
           (:base "xhtml" :path ,(expand-file-name "rnotes/html" g-sites-dir)))
          ("tips"
           (,(expand-file-name "muse/tips" g-docs-dir)
            :default "tips")
            (:base "xhtml" :path ,(expand-file-name "tips/html"
                                                    g-sites-dir)))))



  ;; styles
  (setq muse-xhtml-style-sheet
    "<link rel=\"stylesheet\" type=\"text/css\" href=\"layout.css\" />")

  (unless (assoc "web-xhtml" muse-publishing-styles)
    (muse-derive-style "web-xhtml" "xhtml"
                       :maintainer "guille"
                       :header (expand-file-name
                                 "g-web-xhtml-header-CSS.html"
                                 g-muse-misc-dir)
                       :footer (expand-file-name
                                "g-web-xhtml-footer-CSS.html"
                                 g-muse-misc-dir)))

  (unless (assoc "web-pdf" muse-publishing-styles)
    (muse-derive-style "my-latex" "latex"
                       :header (expand-file-name
                                "g-web-latex-header.tex" g-muse-misc-dir))
    (muse-derive-style "web-pdf" "my-latex"
                       :final   'muse-latex-pdf-generate
                       :browser 'muse-latex-pdf-browse-file
                       :osuffix 'muse-latex-pdf-extension))



  ;; functions to generate a menu
  (defun g-muse-insert-menu ()
    "Insert a menu with the pages of the project"
    (let ((menuitems (mapcar 'car (muse-project-file-alist))))
      (mapconcat 'g-muse-link-href-menu menuitems " ")))

  (defun g-muse-link-href-menu (name)
    "insert a menu item. Use menuactive class for the current page,
menuinactive otherwise"
    (if (string=  name (file-name-nondirectory
                        (or (muse-page-name) "")))
        (concat "<a class=\"menuactive\" href=\""
                (muse-publish-output-name name) "\">" name "</a><br />")
      (concat "<a class=\"menuinactive\" href=\""
              (muse-publish-output-name name) "\">" name "</a><br />")))


  ;; links (based on Jao's)
  (jao-if-darwin
   (defun g-muse-insert-bw-link (f)
     (let ((l (funcall f)))
       (if (car l)
           (progn
             (insert (muse-make-link (car l) (cdr l)))
             (message "Link to %s inserted" (car l)))
         (message "No link found"))))
   (defun jao-muse-insert-safari-link ()
     (interactive)
     (g-muse-insert-bw-link 'g-as-safari-doc))
   (defun jao-muse-insert-firefox-link ()
     (interactive)
     (g-muse-insert-bw-link 'g-as-firefox-doc))
   (defun g-muse-insert-w3m-link ()
     (interactive)
     (g-muse-insert-bw-link 'g-w3m-link))
     
   
   (define-key muse-mode-map (kbd "C-c F") 'jao-muse-insert-firefox-link)
   (define-key muse-mode-map (kbd "C-c S") 'jao-muse-insert-safari-link)
   (define-key muse-mode-map (kbd "C-c W") 'g-muse-insert-w3m-link))


  ;; load experimental dir of Muse for Carbon Emacs
  (jao-if-darwin
   (add-to-list 
    'load-path 
    "/Applications/Emacs.app/Contents/Resources/site-lisp/muse/experimental/"))

  ;; add the experimenta muse-cite
  ;; (require 'muse-cite)
  ;; (add-hook 'muse-before-publish-hook 'muse-cite-munge-footnotes)


;;   (defun g-ssh-publish-project (prj host)
;;     (interactive)
;;     (muse-project-publish prj)
;;     (let ((html-files) 
;;     )


  )





(provide 'g-muse)