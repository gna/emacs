;; helm config

(use-package helm
  :ensure t
  :bind ("C-x f" . helm-for-files)
  :init
  
  ;;(require 'helm-config)

  (setq helm-mode-fuzzy-match t)
  (setq helm-completion-in-region-fuzzy-match t)

  ;; disable margins in scroll completion window
  (setq helm-completion-window-scroll-margin 0)

  (setq helm-bookmark-show-location t))


;; (use-package helm-recoll
;;   :ensure t
;;   :after helm
;;   :defer
;;   :commands helm-recoll
;;   :custom (helm-recoll-directories '(("main" . "~/.recoll"))))



(provide 'gna-helm)
