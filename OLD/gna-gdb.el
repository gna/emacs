;;; -*- lexical-binding: t; -*-


(setq gdb-stack-buffer-addresses t)

(provide 'gna-gdb)
