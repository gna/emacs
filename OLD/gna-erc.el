
(use-package erc
  :ensure t
  :commands (erc erc-tls)
  :custom
  (erc-prompt-for-password nil)
  (erc-prompt-for-nickserv-password nil)

  (erc-rename-buffers t)
  (erc-fill-function 'erc-fill-static)
  (erc-fill-static-center 20)
  (erc-prompt ">")

  (erc-autojoin-channels-alist '(("libera.chat" "#emacs" "#org-mode")
                                 ("oftc" "#debian")))
  (erc-autojoin-timing 'ident)

  (erc-server-reconnect-attempts 5)
  (erc-server-reconnect-timeout 3)

  (erc-hide-list '("JOIN" "PART" "QUIT"))

  (erc-input-line-position -1)

  (erc-header-line-face-method t)
  (erc-join-buffer 'bury)
  (erc-kill-queries-on-quit t)
  
  :preface
  (defun gna-erc ()
    (interactive)
    (erc-tls :server "irc.libera.chat" :port 6697 :nick "gna")
    (erc-tls :server "irc.oftc.net" :port 6697 :nick "gna"))

  :config
  (add-to-list 'erc-modules 'spelling)
  (erc-services-mode 1)
  (erc-update-modules)
  ;; (add-to-list 'erc-mode-hook
  ;;              (lambda ()
  ;;                (set (make-local-variable 'scroll-conservatively) 100)))
  )


(use-package erc-hl-nicks
  :ensure t
  :after erc)


(provide 'gna-erc)
