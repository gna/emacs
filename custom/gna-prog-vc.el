;;; -*- lexical-binding: t; -*-
;; Version control 

(setq vc-handled-backends '(Git SVN))

;; magit
(use-package magit
  :ensure t
  :after nerd-icons
  :custom
  (magit-status-show-hashes-in-headers t)
  (magit-define-global-key-bindings 'recommended)
  (magit-format-file-function #'magit-format-file-nerd-icons))


;; https://github.com/dgutov/diff-hl
(use-package diff-hl
  :ensure t
  :config
  (setq diff-hl-side 'right)
  (add-hook 'dired-mode-hook #'diff-hl-dired-mode)
  (add-hook 'magit-pre-refresh-hook #'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook #'diff-hl-magit-post-refresh)
  (global-diff-hl-mode 1))


;; https://codeberg.org/pidu/git-timemachine
(use-package git-timemachine :ensure t)


(use-package git-modes :ensure t)


(provide 'gna-prog-vc)
