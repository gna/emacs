;;; -*- lexical-binding: t; -*-

(use-package pdf-tools
  :ensure t
  :after transient
  :bind (:map pdf-view-mode-map
              ("M-g g" . pdf-view-goto-page)
              ("M-w" . pdf-view-kill-ring-save))
  :custom
  (pdf-view-display-size 'fit-width)
  (pdf-view-resize-factor 1.1)

  ;; printer
  (pdf-misc-print-program-executable "/usr/bin/gtklp")
  (pdf-misc-print-program-args '("-o sides=two-sided-long-edge"))

  ;; automatically annotate highlights
  (pdf-annot-activate-created-annotations t)
  
  :config
  (pdf-tools-install)
  
  ;; disable whole-line-or-region-mode and bind M-w to pdf-view-kill-ring-save
  (add-hook 'pdf-view-mode-hook (lambda () (whole-line-or-region-local-mode -1)))
  
  (transient-define-prefix gna-transient-pdftools ()
    ["Misc"
     ("a" "Annot. list" pdf-annot-list-annotations)
     ("i" "Info" pdf-misc-display-metadata)
     ("o" "Occur" pdf-occur)
     ("g" "Goto page" pdf-view-goto-page)
     ("m" "Midnight" pdf-view-midnight-minor-mode)
     ("x" "Open external" xah-open-in-external-app)])
  (bind-keys :map pdf-view-mode-map ("/" . gna-transient-pdftools)))


(use-package saveplace-pdf-view
  :ensure t
  :after pdf-tools)


(provide 'gna-pdf-tools)
