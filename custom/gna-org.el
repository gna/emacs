;;; -*- lexical-binding: t; -*-
;;; gna-org --- orgmode related config
;;
;; * Diary:
;; using an org file as diary, we need to have the emacs diary file
;; pointing to the org diary. That is:
;; file .config/emacs/diary with the content "%%(org-diary) /path/to/diary.org"

;;; main org

(use-package org
  :ensure t
  :demand t
  :bind (("C-c a" . org-agenda)
         ("C-c l" . org-store-link))

  :custom
  (org-directory gna-org-dir)
  (org-default-notes-file (expand-file-name "notes.org" org-directory))
  (org-modules '(ol-doi ol-bibtex ol-eshell ol-eww ol-info ol-irc org-id)))


;;;; org main config (visual / functional)

(setq org-special-ctrl-a/e t)
(setq org-imenu-depth 5)

;; default to indent-mode
(setq org-adapt-indentation nil)
(setq org-startup-indented t)

(setq org-startup-folded t)
(setq org-startup-with-inline-images t)
(setq org-image-actual-width 400)

(setq org-hide-emphasis-markers t)
(setq org-hide-leading-stars t)

(setq org-list-allow-alphabetical t)

(setq org-catch-invisible-edits 'smart)
(setq org-cycle-separator-lines 0)
(setq org-use-speed-commands t)
(setq org-support-shift-select t)

(setq org-refile-targets '((org-agenda-files :maxlevel . 2)))
(setq org-refile-use-outline-path t)
(setq org-outline-path-complete-in-steps nil)

(plist-put org-format-latex-options :scale 1.5)
(setq org-highlight-latex-and-related '(latex script))

;; enable cdlatex
(add-hook 'org-mode-hook 'turn-on-org-cdlatex)

;; enable org-crypt
(require 'org-crypt)
(org-crypt-use-before-save-magic)

;; attachments
(setq org-attach-id-dir "org-data")
(setq org-attach-store-link-p 'attached)


;;;; calendar / agenda / diary / appts

(setq calendar-week-start-day 1)
(setq calendar-holidays nil)
(setq calendar-mark-diary-entries-flag t)
(setq calendar-date-style 'iso)

(setq org-todo-keywords
      '((sequence "TODO(t)" "WAIT(w!)" "|" "DONE(d!)" "CANCEL(c!)")))
(setq org-enforce-todo-dependencies t)
(setq org-log-done 'time)
(setq org-log-redeadline 'time)
(setq org-log-reschedule 'time)

(setq gna-org-agenda-dir (expand-file-name "agenda" org-directory))

(setq org-agenda-diary-file (expand-file-name "diary.org" gna-org-agenda-dir))
(setq org-agenda-files (list gna-org-agenda-dir))
(setq org-agenda-default-appointment-duration 60)
(setq org-agenda-include-diary t)
(setq org-agenda-include-inactive-timestamps nil)
(setq org-agenda-restore-windows-after-quit t)
(setq org-agenda-start-on-weekday 1)
(setq org-agenda-span 14)
(setq org-agenda-window-setup 'current-window)

(setq org-columns-default-format
      "%25ITEM %TODO %3PRIORITY %DEADLINE %TAGS")

;; in agenda view separate each week with an horizontal line (placed before
;; monday)
(setq org-agenda-format-date
      (lambda (date)
        (if (eq (calendar-day-of-week date) 1)
            (concat
             (make-string (window-width) 9472) "\n"
             (org-agenda-format-date-aligned date))
          (org-agenda-format-date-aligned date))))

;; generate ics file 
(setq org-icalendar-combined-agenda-file
      (expand-file-name "agenda/org.ics" org-directory))
(setq org-icalendar-use-scheduled '(todo-start event-if-todo))

;; export to ical after capture
(add-hook 'org-capture-after-finalize-hook
          #'org-icalendar-combine-agenda-files)

;; activate appointments after save
(add-hook 'org-agenda-finalize-hook (lambda () (org-agenda-to-appt t)))
(appt-activate t)

;; appointments
(use-package appt
  :ensure alert
  :custom
  (appt-message-warning-time 15)
  (appt-display-interval 5)
  (appt-display-diary nil)
  (appt-display-mode-line t)
  (appt-display-format 'window)
  (appt-disp-window-function #'gna-notify-appt)
  :config
  (defun gna-notify-appt (min-to-app curr-time msg)
    (alert msg :title (format "In %s min." min-to-app)
           :severity 'high :category 'org))
  (appt-activate 1))


;;;; org file apps

(setq org-file-apps
      '(("\\.eml\\'" . "thunderbird -file %s")
        ("\\.x?html?\\'" . default)
        (t . emacs)))

;; add link to open in xournalpp
(org-link-set-parameters
 "xournalpp"
 :follow (lambda (link)
           (let ((process-connection-type nil))
             (start-process "org-xournalpp" nil "/usr/bin/xournalpp" link))))


;;;; org babel

(setq org-confirm-babel-evaluate nil)
(setq org-babel-python-command "python3")

(org-babel-do-load-languages
 'org-babel-load-languages
 '((calc . t)
   (ditaa . t)
   (emacs-lisp . t)
   (latex . t)
   (python . t)
   (R . t)
   (scheme . t)
   (shell . t)))


;;; org exporters

(use-package htmlize :ensure t)

;; html exporter
(use-package ox-html
  :after org
  :custom
  (org-html-validation-link nil)
  (org-html-doctype "html5")
  (org-html-html5-fancy t))


;; html bootstrap (https://github.com/marsmining/ox-twbs)
(use-package ox-twbs
  :ensure t)


;; Markdown exporter
(use-package ox-md :after org)


;; Latex exporter
(use-package ox-latex
  :after org
  :custom
  (org-latex-prefer-user-labels t)
  :config
  (setq org-latex-listings 'minted)
  (add-to-list 'org-latex-packages-alist '("" "minted"))
  (setq org-latex-pdf-process
        (list "latexmk -shell-escape -bibtex -f -pdf %f")))


;; Beamer exporter
(use-package ox-beamer :after org)


;;; org capture

(use-package org-capture
  :after org denote
  :bind ("C-c r" . org-capture)

  :custom
  (org-capture-bookmark nil)
  (org-capture-templates
   '(("t" "TODO" entry (file+headline "agenda/agenda.org" "Refile")
      "* TODO %?\nSCHEDULED: %^t\n:PROPERTIES:\n:DATE: %U\n:END:\n"
      :empty-lines-after 1)
     ("n" "New note (with Denote)" plain (file denote-last-path)
      #'denote-org-capture
      :no-save t
      :immediate-finish nil
      :kill-buffer t
      :jump-to-captured t)
     ("a" "Appointment")
     ("at" "Teaching App" entry
      (file+olp+datetree "agenda/diary.org" "Teach")
      "* %? \n:PROPERTIES:\n:CATEGORY: Teach\n:org-gcal:\n%T\n:END:"
      :time-prompt t)
     ("ar" "Research App" entry
      (file+olp+datetree "agenda/diary.org" "Research")
      "* %? \n:PROPERTIES:\n:CATEGORY: Research\n:org-gcal:\n%T\n:END:"
      :time-prompt t)
     ("ag" "Gestion App" entry
      (file+olp+datetree "agenda/diary.org" "Gestion")
      "* %? \n:PROPERTIES:\n:CATEGORY: Gestion\n:org-gcal:\n%T\n:END:"
      :time-prompt t)
     ("ap" "Personal App" entry
      (file+olp+datetree "agenda/diary.org" "Personal")
      "* %? \n:PROPERTIES:\n:CATEGORY: Personal\n:org-gcal:\n%T\n:END:"
      :time-prompt t))))


(use-package org-protocol
  :after (org eww)
  :config
  ;; open firefox link in eww
  ;;https://marcowahl.gitlab.io/emacs-blog-mw/2021/20210126.html
  (add-to-list 'org-protocol-protocol-alist
               '("eww"
                 :protocol "eww"
                 :function mw-start-eww-for-url))
  (defun mw-start-eww-for-url (plist)
    "Raise Emacs and call eww with the url in PLIST."
    (raise-frame)
    (eww (plist-get plist :url))
    nil))


;;; org other packages

(use-package org-gcal
  :ensure t
  :defer t
  :commands gna-enable-org-gcal-sync
  :config
  (setq org-gcal-client-id 
        (auth-source-pick-first-password :host "google-main-calendar"
                                         :user "client-id"))
  (setq org-gcal-client-secret
        (auth-source-pick-first-password :host "google-main-calendar"
                                         :user "client-secret"))
  (setq org-gcal-file-alist
        (mapcar (lambda (pair)
                  (cons (auth-source-pick-first-password
                         :host "google-main-calendar"
                         :user (car pair))
                        (expand-file-name (cdr pair) gna-org-agenda-dir)))
                '(("calendar-id-personal" . "g-personal.org")
                  ("calendar-id-birthday" . "g-birthday.org")
                  ("calendar-id-outlook-uab" . "g-uab.org"))))
  (org-gcal-reload-client-id-secret)

  (require 'plstore)
  (setq plstore-cache-passphrase-for-symmetric-encryption t)
  (add-to-list 'plstore-encrypt-to "2ECF56A7FF2299DA")

  (setq org-gcal-local-timezone "Europe/Madrid")
  (setq org-gcal-notify-p nil)

  (defun gna-org-gcal-sync-clear-token ()
    "Sync calendar, clearing tokens first."
    (interactive)
    (when org-gcal--sync-lock
      (warn "%s" "‘org-gcal--sync-lock’ not nil - calling ‘org-gcal--sync-unlock’.")
      (org-gcal--sync-unlock))
    (org-gcal-sync-tokens-clear)
    (org-gcal-sync)
    nil)
  
  (defun gna-enable-org-gcal-sync ()
    "Run org-gcal-sync every hour"
    (interactive)
    (run-at-time nil (* 60 60) #'gna-org-gcal-sync-clear-token)))


;; https://github.com/spegoraro/org-alert
(use-package org-alert
  :ensure t
  :after (org)
  :custom (alert-default-style 'notifications))


;; https://github.com/awth13/org-appear
(use-package org-appear
  :ensure t
  :after (org)
  :hook (org-mode . org-appear-mode))


;; https://github.com/io12/org-fragtog
(use-package org-fragtog
  :ensure t
  :after (org)
  :hook (org-mode . org-fragtog-mode)
  :custom
  (org-fragtog-ignore-predicates
   '(org-at-table-p org-at-table.el-p org-at-block-p)))


;; https://github.com/tarsius/orglink
(use-package orglink
  :ensure t
  :after (org)
  :diminish orglink-mode
  :config (global-orglink-mode 1))


;; https://github.com/alphapapa/org-web-tools
(use-package org-web-tools
  :ensure t
  :demand t
  :after (org)
  :bind (:map org-mode-map
              ("C-c i" . org-web-tools-insert-link-for-url)))


(use-package org-download
  :ensure t
  :after (org)
  (org-download-enable))


;; https://github.com/shg/org-inline-pdf.el
(use-package org-inline-pdf
  :ensure t
  :hook (org-mode . org-inline-pdf-mode))


;;; org custom 

(defun gna-org-copy-link-url-at-point ()
  (interactive)
  (let* ((elem (org-element-context))
         (type (org-element-type elem))
         (link-str (org-element-property :raw-link elem)))
    (if (eq type 'link)
        (progn (kill-new link-str)
               (message "Copied URL: %s" link-str))
      (message "No URL at point"))))


;;; org presentations

;; https://github.com/takaxp/org-tree-slide
(use-package org-tree-slide
  :ensure t
  :bind (
         :map org-mode-map
         ("<f4>" . org-tree-slide-mode)
         :map org-tree-slide-mode-map
         ("C-<right>" . org-tree-slide-move-next-tree)
         ("C-<left>" . org-tree-slide-move-previous-tree)
         ("C-<up>" . text-scale-increase)
         ("C-<down>" . text-scale-decrease)))

;; https://github.com/joostkremers/writeroom-mode
(use-package writeroom-mode :ensure t)


;;; org transient

(with-eval-after-load "transient"
  (transient-define-prefix gna-transient-org ()
    [:class transient-columns
            ["notes"
             ("d" "create note" denote)
             ("s" "search notes" consult-notes)
             ("f" "rename file from front matter" denote-rename-file-using-front-matter)
             ("i" "insert link to note" denote-link)
             ("b" "backlinks" denote-link-backlinks)
             ("g" "grep" consult-denote-ripgrep)
             ("r" "rename file" denote-rename-file)]
            ["capture"
             ("r" "Capture" org-capture)
             ("l" "Last captured" org-capture-goto-last-stored)]
            ["misc"
             ("w" "Copy link" gna-org-copy-link-url-at-point)]]))

(global-set-key (kbd "<f5>") #'gna-transient-org)


(provide 'gna-org)
;;; gna-org.el ends here
