;;; -*- lexical-binding: t; -*-
;;
;; Modus Themes: https://protesilaos.com/modus-themes/
;;
;; Other nice themes:
;; - zenburn: https://github.com/bbatsov/zenburn-emacs
;; - spacemacs: https://github.com/nashamri/spacemacs-theme
;;


(use-package modus-themes
  :ensure t
  :demand t
  :bind ("<f6>" . modus-themes-toggle)
  :config
  (setq modus-themes-completions
        '((matches . (background))
          (selection . (accented))
          (popup . (accented))))

  (setq modus-themes-common-palette-overrides
        '((fringe bg-active)
          (fg-region unspecified)
          (bg-mode-line-active bg-blue-nuanced)))

  (load-theme 'modus-operandi :no-confirm))

(provide 'gna-themes)
