;;; -*- lexical-binding: t; -*-

(use-package avy
  :ensure t
  :bind ("C-c SPC" . avy-goto-char-timer))


(provide 'gna-avy)
