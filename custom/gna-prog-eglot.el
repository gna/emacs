;;; -*- lexical-binding: t; -*-
;;; gna-prog-eglot.el --- 


(use-package eglot
  :ensure t
  :after (eldoc-box cape codeium)
  :bind (:map eglot-mode-map
              (("C-c c" . eglot-code-actions)
               ("C-c C-r" . eglot-rename)
               ("C-h ." . eldoc-box-help-at-point)))
  :hook ((eglot-managed-mode-hook . eglot-inlay-hints-mode))
  :config
  (add-to-list 'eglot-ignored-server-capabilities :hoverProvider)
  ;; (add-hook 'eglot-managed-mode-hook #'eldoc-box-hover-mode t)
  )


(use-package consult-eglot
  :ensure t)


;; https://github.com/emacs-languagetool/eglot-ltex
(use-package eglot-ltex
  :load-path (lambda () (expand-file-name "eglot-ltex" gna-local-lisp-dir))
  :init
  (setq eglot-ltex-server-path "~/applications/ltex-ls/"))


(use-package sideline-eglot
  :load-path (lambda () (expand-file-name "sideline-eglot" gna-local-lisp-dir))
  :init
  (use-package sideline :ensure t)
  (setq sideline-backends-right '(sideline-eglot)))



(provide 'gna-prog-eglot)
;;; gna-prog-eglot.el ends here
