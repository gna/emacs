;;; -*- lexical-binding: t; -*-


(use-package js
  :custom
  (js-indent-level 2)
  (js-switch-indent-offset 2))


(provide 'gna-prog-javascript)
