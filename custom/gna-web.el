;;; -*- lexical-binding: t; -*-
;; web browsing and eww config


(defun gna-browse-url-firefox-profile (profile)
  "Return a handler function to open a url in firefox with a given
profile. Based on:
https://www.n16f.net/blog/controlling-link-opening-in-emacs/"
  (lambda (url &rest args)
    (let* ((clean-url (browse-url-encode-url url))
           (process-environment (browse-url-process-environment))
           (process-name (concat "firefox " clean-url))
           (process-args `("--new-tab" ,clean-url
                           ,@(if profile (list "-P" profile)))))
      (apply #'start-process process-name nil "firefox" process-args))))

(setq browse-url-browser-function 'browse-url-handlers)
(setq browse-url-handlers
      `(("^https://accounts.google.com" .
         ,(gna-browse-url-firefox-profile "google-main"))
        ("^https://cv.uab.cat" . ,(gna-browse-url-firefox-profile "UAB"))
        ("^https://\\w+.-my.sharepoint.com" . ,(gna-browse-url-firefox-profile "UAB"))
        ("^https://teams.microsoft.com" . browse-url-chrome)
        ("^https://zoom.us" . browse-url-chrome)
        ("." . eww-browse-url)))


(setq browse-url-generic-program (executable-find "firefox"))
(setq browse-url-secondary-browser-function 'browse-url-firefox)


(use-package shr
  :custom
  (shr-inhibit-images t)
  (shr-use-fonts nil))


(use-package eww
  :defer t
  :bind (("C-x j" . 'browse-url-at-point)))


(use-package ace-link
  :ensure t
  :after eww
  :bind (:map eww-mode-map
              ("f" . ace-link-eww)))


(provide 'gna-web)

