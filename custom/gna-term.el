;;; -*- lexical-binding: t; -*-
;; eshell, vterm, ...
;; Guillermo Navarro
;;
;; - outline-minor mode for eshell: gna-outline.org
;;

;;; eshell
(use-package eshell
  :demand t
  :custom
  (eshell-aliases-file
   (expand-file-name "misc/eshell.alias" gna-emacs-config-dir))
  (eshell-rm-interactive-query t)
  (eshell-scroll-to-bottom-on-input t)
  
  ;; ignore case for filename completion
  (eshell-cmpl-ignore-case t)

  ;; history
  (eshell-buffer-maximum-lines 20000)
  (eshell-history-size 10000)
  ;; bug#63360 (fixed for 30.1):
  ;; https://git.savannah.gnu.org/cgit/emacs.git/commit/?id=7b0f24ab1f9770408a08181ab9f8ac2b43e5ab9b)
  ;; (eshell-hist-ignoredups 'erase)

  :config
  ;; visual commands
  (require 'em-term)
  (add-to-list 'eshell-visual-commands "aptitude")
  (add-to-list 'eshell-visual-commands "watch")
  (setq eshell-visual-subcommands '(("git" "log" "diff" "show")))
  (setq eshell-visual-options '(("git" "--help" "--paginate")))

  (require 'esh-toggle))


;;;###autoload
(defun gna-goto-eshell (arg)
  (interactive "P")
  (if arg
      (eshell-toggle-cd)
    (if (project-current)
        (project-eshell)
      (eshell-toggle nil))))


(use-package eshell-syntax-highlighting
  :ensure t
  :after eshell
  :config
  (eshell-syntax-highlighting-global-mode +1))


(use-package eshell-prompt-extras
  :ensure t
  :after eshell
  :config
  (with-eval-after-load "esh-opt"
    (autoload 'epe-theme-lambda "eshell-prompt-extras")
    (setq eshell-prompt-function 'epe-theme-multiline-with-status)))


;; https://github.com/peterwvj/eshell-up
(use-package eshell-up
  :ensure t
  :after eshell
  :custom
  (eshell-up-print-parent-dir t))


(use-package bash-completion :ensure t)


;; https://github.com/lemonbreezes/emacs-fish-completion
(use-package fish-completion
  :ensure t
  :config
  (setq fish-completion-fallback-on-bash-p t)
  (global-fish-completion-mode))


(use-package capf-autosuggest
  :ensure t
  :hook (eshell-mode . capf-autosuggest-mode))


;;; eat
(use-package eat
  :ensure t
  :hook ((eshell-mode . eat-eshell-mode)
         (eshell-mode . eat-eshell-visual-command-mode))
  :diminish (eat-eshell-mode)
  :custom
  (eat-kill-buffer-on-exit t)
  (eat-enable-yank-to-terminal t))



(provide 'gna-term)
