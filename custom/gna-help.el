;;; -*- lexical-binding: t; -*-

(use-package helpful
  :ensure t
  :bind (("C-h f" . helpful-callable)
         ("C-h k" . helpful-key)
         ("C-h o" . helpful-symbol)
         ("C-h v" . helpful-variable)))


(provide 'gna-help)
