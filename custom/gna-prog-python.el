;;; -*- lexical-binding: t; -*-
;; python


(use-package python
  :hook ((python-ts-mode . eglot-ensure))
  :init
  (add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode))
  
  :config
  (setq python-shell-interpreter "python3")
  (setq python-interpreter "python3")

  ;; keep the prompt at the bottom
  (add-hook 'inferior-python-mode-hook
            (lambda ()
              (setq comint-move-point-for-output t))))


(use-package pyvenv
  :ensure t
  :config
  (pyvenv-mode 1))


(use-package python-black
  :after python
  :ensure t
  :custom
  (python-black-extra-args '("--line-length" "80")))


(use-package yapfify
  :ensure t
  :custom
  (yapfify-executable "yapf3"))


(use-package py-autopep8 :ensure t)


(use-package pip-requirements :ensure t)


;; https://github.com/glyph/python-docstring-mode
(use-package python-docstring
  :ensure t
  :diminish
  :hook (python-mode . python-docstring-mode))


;; https://github.com/naiquevin/sphinx-doc.el
(use-package sphinx-doc
  :ensure t)


;; sagemath
(use-package sage-shell-mode
  :ensure t
  :hook
  (sage-shell-mode . eldoc-mode)
  (sage-shell:sage-mode .eldoc-mode)

  :init
  (sage-shell:define-alias))


;; org-babel integration https://github.com/sagemath/ob-sagemath
(use-package ob-sagemath
  :after org
  :ensure t
  :custom
  (org-babel-default-header-args:sage '((:session . t)
                                        (:results . "output"))))


;; (use-package cython-mode :ensure t)


(provide 'gna-prog-python)
