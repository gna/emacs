;;; -*- lexical-binding: t; -*-
;;; generic editing stuff
;;;

;;; backups
(setq version-control t)
(setq backup-by-copying t)
(setq backup-directory-alist
      `(("." . ,(expand-file-name "backup" user-emacs-directory))))
(setq delete-old-versions t)
(setq kept-new-versions 5)
(setq kept-old-versions 2)


;; https://github.com/lewang/backup-walker
(use-package backup-walker
  :ensure t
  :commands backup-walker-start)

;;; editing
;; text-mode is the default major mode
(setq-default major-mode 'text-mode)

;; kill whole line if at the beg.
(setq kill-whole-line t)

;; spaces and tabs
(setq-default indent-tabs-mode nil)
(setq indent-tabs-width 4)

;; sentences separated by just one space
(setq sentence-end "[.?!][]\"')]*\\($\\|\t\\| \\)[ \t\n]*")
(setq sentence-end-double-space nil)

;; clipboard
(setq select-enable-clipboard t)
(setq select-enable-primary t)

;; show wrapped lines in visual-line-mode
(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

;; autofill at 80
(setq-default fill-column 79)
(add-hook 'text-mode-hook 'turn-on-auto-fill)
(setq comment-auto-fill-only-comments nil)

;; fill-column-indicator
(setq-default display-fill-column-indicator-column 80)
(add-hook 'prog-mode-hook #'display-fill-column-indicator-mode)
(add-hook 'text-mode-hook #'display-fill-column-indicator-mode)
(add-hook 'conf-mode-hook #'display-fill-column-indicator-mode)

;; save place in each file (file defaults to ~/.emacs.d/places)
(save-place-mode 1)

;; ediff
(setq ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-split-window-function 'split-window-horizontally)
(add-hook 'ediff-after-quit-hook-internal 'winner-undo)

;; time stamps: update time stamp template if present in the first 8 lines of a
;; file as Time-stamp: <> or Time-stamp: " "
(add-hook 'before-save-hook 'time-stamp) ; update when saving


;; highlight current line in some modes with LIN
;; https://protesilaos.com/emacs/lin
(use-package lin
  :ensure t
  :config
  (setq lin-mode-hooks
        '(archive-mode-hook
          bookmark-bmenu-mode-hook
          conf-mode-hook
          dired-mode-hook
          grep-mode-hook
          ibuffer-mode-hook
          magit-log-mode-hook
          org-agenda-mode-hook
          occur-mode-hook
          package-menu-mode-hook
          proced-mode-hook
          tar-mode-hook))
  (lin-global-mode 1))


;; unfill paragraph with second call (two M-q)
(use-package unfill
  :ensure t
  :config
  (global-set-key [remap fill-paragraph] #'unfill-toggle))


(defun gna-yank-as-comment ()
  "yank and comment"
  (interactive)
  (save-excursion
    (let ((beg (point)))
      (yank)
      (comment-region beg (point)))))
(global-set-key (kbd "C-c y") 'gna-yank-as-comment)


(use-package visual-fill-column
  :ensure t
  :config
  (add-hook 'visual-fill-column-mode-hook #'visual-line-mode))


(use-package aggressive-indent
  :ensure t
  :config
  (global-aggressive-indent-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'web-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'python-ts-mode))


;; multiple-cursors
(use-package multiple-cursors
  :ensure t
  :bind (("C-S-c C-S-c" . mc/edit-lines)
         ("C-S-c ." . mc/mark-all-like-this)))


;; ;; TODO this does not play nicely with lispy!
;; ;; https://github.com/andreyorst/region-bindings.el
(use-package region-bindings-mode
  :ensure t
  :bind (:map region-bindings-mode-map
              ("n" . mc/mark-next-symbol-like-this)
              ("N" . mc/mark-next-like-this)
              ("p" . mc/mark-previous-symbol-like-this)
              ("P" . mc/mark-previous-like-this)
              ("a" . mc/mark-all-symbols-like-this)
              ("A" . mc/mark-all-like-this)
              ("s" . mc/mark-all-in-region-regexp)
              ("l" . mc/edit-ends-of-lines))
  :config
  (region-bindings-mode-enable))


;; whole-line-or-region (act on current line if no region is active)
(use-package whole-line-or-region
  :ensure t
  :diminish whole-line-or-region-local-mode 
  :config
  (whole-line-or-region-global-mode))


;; anzu (for query/replace, etc.)
(use-package anzu
  :ensure t
  :bind (("M-%" . anzu-query-replace)
         ("C-M-%" . anzu-query-replace-regexp))
  :config
  (global-anzu-mode t)
  (setq anzu-mode-lighter ""))


;; https://github.com/casouri/vundo
(use-package vundo
  :ensure t
  :custom
  (vundo-glyph-alist vundo-unicode-symbols)
  :config
  (set-face-attribute 'vundo-default nil :family "Symbola")
  :bind (("C-?" . vundo)
         ("C-x u" . vundo)))


;; https://github.com/tarsius/hl-todo
(use-package hl-todo
  :ensure t
  :hook ((latex-mode . hl-todo-mode)
         (LaTeX-mode . hl-todo-mode)
         (prog-mode . hl-todo-mode)))


;; show color codes as the actual code
(use-package rainbow-mode
  :ensure t
  :defer t)


(use-package expand-region
  :ensure t
  :bind ("C-=" . er/expand-region))


(use-package goto-chg
  :ensure t
  :bind (("C-z" . goto-last-change)))


;; GIFT format for quizes (moodle)
(use-package gift-mode
  :ensure t
  :config
  (add-hook 'gift-mode-hook 'turn-off-auto-fill))


;; https://github.com/alpha22jp/atomic-chrome
;; used with https://ghosttext.fregante.com/
;; need to run atomic-chrome-start-server / atomic-chrome-stop-server
(use-package atomic-chrome :ensure t)



(provide 'gna-edit)
