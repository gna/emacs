;;; -*- lexical-binding: t; -*-
;; lisp, scheme and co.
;; guille


(use-package geiser
  :ensure t
  :hook (scheme-mode . geiser-mode)
  :custom
  (geiser-repl-history-filename
   (expand-file-name "geiser-history" user-emacs-directory))
  (geiser-default-implementation 'guile))


(use-package geiser-guile :ensure t)
(use-package geiser-racket :ensure t)


(provide 'gna-prog-lisp)
