;;; -*- lexical-binding: t; -*-
;; auctex, reftex

(use-package tex
  :defer t
  :ensure auctex
  :config

  ;; revert pdf buffer after compilation
  (add-hook 'TeX-after-compilation-finished-functions
            #'TeX-revert-document-buffer)

  ;; use pdf-tools for View
  (setq TeX-view-program-selection '((output-pdf "PDF Tools")))
  (setq TeX-source-correlate-start-server t)

  ;; enable parse on load and parse on save
  (setq TeX-parse-self t)
  (setq TeX-auto-save t)
  (setq TeX-auto-local ".tex-auto-local")

  (add-to-list 'TeX-command-list '("Make" "make" TeX-run-compile nil t))

  ;; save file before command without asking
  (setq TeX-save-query nil)
  
  ;; query for master file
  (setq-default TeX-master 'dwin)

  ;; unset C-c # in TeX-mode-map (we use it for window management)
  (define-key TeX-mode-map (kbd "C-c #") nil)

  (use-package latex
    :defer t
    :mode ("\\.tikz$" . LaTeX-mode)
    :config
    (add-hook 'LaTeX-mode-hook #'turn-on-reftex)
    (add-hook 'LaTeX-mode-hook #'LaTeX-math-mode)
    (add-hook 'LaTeX-mode-hook #'TeX-source-correlate-mode)
    (setq LaTeX-math-menu-unicode t)
    ;; fix outline for exam class
    (add-to-list 'TeX-outline-extra '("[ \t]*\\\\question\\b" 5) t)
    (add-to-list 'TeX-outline-extra '("[ \t]*\\\\part\\b" 7) t)))


(use-package cdlatex
  :ensure t
  :config
  (setq cdlatex-simplify-sub-super-scripts nil)
  (add-hook 'cdlatex-tab-hook
            (defun cdlatex-indent-maybe ()
              (when (or (bolp) (looking-back "^[ \t]+"))
                (LaTeX-indent-line)
                t)))
  (add-hook 'LaTeX-mode-hook 'turn-on-cdlatex))


(use-package bibtex
  :custom
  ;; bibtex-mode autokey format
  (bibtex-autokey-additional-names ".etal.")
  (bibtex-autokey-name-separator ".")
  (bibtex-autokey-names 1)
  (bibtex-autokey-names-stretch 1)
  (bibtex-autokey-year-length 4)
  (bibtex-autokey-titlewords 5)
  (bibtex-autokey-titleword-length 'infty))


(use-package reftex
  :after tex
  :custom
  (reftex-enable-partial-scans t)
  (reftex-save-parse-info t)
  (reftex-use-multiple-selection-buffers t)
  (reftex-plug-into-AUCTeX t)

  (reftex-default-bibliography gna-bib-zotero-files)
  
  ;; Automatically insert non-breaking space before citation
  ;; (https://tex.stackexchange.com/questions/399474/automatic-tilde-before-cite-in-reftex)
  (reftex-format-cite-function 
   '(lambda (key fmt)
      (let ((cite (replace-regexp-in-string "%l" key fmt)))
        (if (or (= ?~ (string-to-char fmt))
                (member (preceding-char) '(?\ ?\t ?\n ?~ ?{ ?,))
                (member (following-char) '(?} )))
            cite
          (concat "~" cite))))))


(use-package biblio
  :ensure t)


(provide 'gna-latex)
  
