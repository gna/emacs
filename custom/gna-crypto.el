;;; -*- lexical-binding: t; -*-
;; guille

;;; easy pg  
(use-package epa-file
  :config
  (epa-file-enable))

(setq epg-pinentry-mode 'loopback)


;;; auth-source
(use-package auth-source
  :config
  (add-to-list 'auth-source-protocols '(local "local"))
  (auth-source-pass-enable)
  (add-to-list 'auth-sources 'password-store)
  (add-to-list 'auth-sources
               (expand-file-name "authinfo.gpg" gna-emacs-config-dir)))


;;; password store 
(use-package password-store-menu
  :ensure t
  :config (password-store-menu-enable)
  :custom (password-store-menu-key "C-c s"))


(provide 'gna-crypto)
