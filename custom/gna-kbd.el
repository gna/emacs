;;; -*- lexical-binding: t; -*-
;; Keyboard (global keybindings)
;; keybindings for specific modes are usually defined in its use-package

;;; Keyboard input 
;; (setq default-input-method "spanish-prefix")
;; (setq default-input-method "latin-1-prefix")
(setq default-input-method "catalan-prefix")
(customize-set-variable 'default-transient-input-method "TeX")

;;; global keys
(global-set-key (kbd "<f10>") 'dired-jump)
(global-set-key (kbd "<f9>") 'gna-goto-eshell)

(global-set-key (kbd "C-x <prior>") 'beginning-of-buffer)
(global-set-key (kbd "C-x <next>") 'end-of-buffer)

(global-set-key (kbd "M-o") 'other-window)

;;; which-key
(use-package which-key
  :ensure t
  :diminish which-key-mode
  :custom
  (which-key-add-column-padding 2)
  (which-key-max-display-columns 3)
  :config
  (which-key-mode))


;;; transient
(use-package transient
  :demand t
  :config
  (transient-bind-q-to-quit))


;;; repeat-mode
(use-package repeat
  :config
  (repeat-mode))


(provide 'gna-kbd)


