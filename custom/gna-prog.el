;;; -*- lexical-binding: t; -*-
;; generic programming config

(setq comint-move-point-for-output t) 

;;; compile
(use-package compile
  :bind (("C-c c" . compile))
  :custom
  (compilation-scroll-output t)

  :config
  (defun endless/colorize-compilation ()
    "Colorize from `compilation-filter-start' to `point'."
    (let ((inhibit-read-only t))
      (ansi-color-apply-on-region
       compilation-filter-start (point))))
  (add-hook 'compilation-filter-hook #'endless/colorize-compilation))


;;; flymake
(use-package flymake
  :commands flymake-mode
  :custom
  (flymake-mode-line-format
   '("" flymake-mode-line-exception flymake-mode-line-counters))
  :bind (:map flymake-mode-map
              ("M-m" . gna-transient-flymake)
              ("C-c f n" . flymake-goto-next-error)
              ("C-c f p" . flymake-goto-prev-error)
              ("C-c f i" . flymake-show-diagnostic)
              ("C-c f b" . flymake-show-buffer-diagnostics))

  :config
  (transient-define-prefix gna-transient-flymake ()
    [["Flymake fix"
      ("n" "next error" flymake-goto-next-error :transient t)
      ("p" "previous error" flymake-goto-prev-error :transient t)
      ("SPC" "eglot code action" eglot-code-actions :transient t)]
     ["Flymake info"
      ("b" "show diagnostics" flymake-show-buffer-diagnostics)
      ("i" "show diagnostic" flymake-show-diagnostic)
      ("c" "consult flymake" consult-flymake)]]))


;;; shell
(use-package flymake-shellcheck
  :ensure t
  :commands flymake-shellcheck-load
  :init
  (add-hook 'sh-mode-hook 'flymake-shellcheck-load))

;; indent 2 spaces
(setq sh-basic-offset 2)


;;; eldoc 
(use-package eldoc
  :diminish eldoc-mode)


(use-package eldoc-box
  :ensure t
  :diminish eldoc-box-hover-mode
  :custom
  (eldoc-box-clear-with-C-g t))


;;; lispy
(use-package lispy
  :ensure t
  :config
  (add-hook 'emacs-lisp-mode-hook (lambda () (lispy-mode 1))))


;;; smartscan https://github.com/mickeynp/smart-scan
(use-package smartscan
  :ensure t
  :commands smartscan-mode
  :init (add-hook 'prog-mode-hook #'smartscan-mode))


;;; iedit https://github.com/victorhge/iedit
(use-package iedit :ensure t)


;;; xref
(if (executable-find "rg")
    (setq xref-search-program 'ripgrep))


;;; json-mode
(use-package json-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.json$" . json-mode))
  (add-to-list 'auto-mode-alist '("\\.jsonc$" . json-mode)))

(provide 'gna-prog)
