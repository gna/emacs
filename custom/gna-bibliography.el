;;; -*- lexical-binding: t; -*-
;; bibliography management
;; mainly org related

(use-package citeproc
  :ensure t)


(use-package oc
  :after (org)
  :custom
  (org-cite-csl-styles-dir "~/Documents/bibliography/Zotero/styles")
  :init
  (require 'oc-basic) 
  (require 'oc-biblatex)
  (require 'oc-bibtex)
  (require 'oc-csl)
  (require 'oc-natbib)
  :config
  (when gna-bib-zotero-files
    (setq org-cite-global-bibliography gna-bib-zotero-files)))


;; https://github.com/emacs-citar/citar
(use-package citar
  :ensure t
  :after (org tex)
  :bind (:map org-mode-map :package org
              ("C-c b" . #'org-cite-insert))
  :hook
  (LaTeX-mode . citar-capf-setup) 
  (org-mode . citar-capf-setup)
  :custom
  ;; org-cite
  (citar-bibliography org-cite-global-bibliography)
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar)
  (citar-at-point-function 'embark-act)
  (citar-notes-paths (list gna-bib-notes-dir)))


(use-package citar-embark
  :after (citar embark)
  :ensure t
  :diminish
  :config
  (citar-embark-mode))


;; https://github.com/pprevos/citar-denote
(use-package citar-denote
  :ensure t
  :after (citar denote)
  :diminish
  :custom
  (citar-denote-subdir t)
  (citar-denote-title-format "author-year-title")
  :init
  (citar-denote-mode))


(provide 'gna-bibliography)
