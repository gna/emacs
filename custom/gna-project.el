;;; gna-project.el ---                             -*- lexical-binding: t; -*-


(use-package project
  :custom
  (project-vc-extra-root-markers '(".project.el" ".projectile"))
  :config
  (add-to-list 'project-switch-commands '(magit-project-status "Magit" ?m)))


(provide 'gna-project)
;;; gna-project.el ends here
