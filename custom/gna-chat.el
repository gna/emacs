;;  -*- lexical-binding: t; -*-

(use-package mastodon
  :ensure t
  :custom
  (mastodon-instance-url "https://mastodon.social")
  (mastodon-active-user "guillermo.navarro@gmail.com"))


(use-package circe
  :ensure t
  :commands (gna-irc)
  :custom
  (circe-network-options
   `(("Libera"
      :host "irc.libera.chat" :port 6697 :tls t
      :nick ,(gna-private-get-user "irc.libera.chat")
      :sasl-username ,(gna-private-get-user "irc.libera.chat")
      :sasl-password ,(gna-private-get-secret "irc.libera.chat")
      :channels ("#emacs" "#org-mode" "#systemcrafters"))
     ("OFTC"
      :host "irc.oftc.net" :port 6697 :tls t
      :nick ,(gna-private-get-user "irc.oftc.net")
      :nickserv-password ,(gna-private-get-secret "irc.oftc.net")
      :sasl-username ,(gna-private-get-user "irc.oftc.net")
      :sasl-password ,(gna-private-get-secret "irc.oftc.net"))))

  (circe-default-part-message "")
  (circe-default-quit-message "")
  (circe-reduce-lurker-spam t)
  (circe-server-auto-join-default-type :after-auth)

  (lui-track-indicator 'fringe)
  (lui-scroll-behavior 'post-output)
  
  :config
  (enable-circe-color-nicks)
  (enable-circe-display-images)
  (enable-lui-track)
  (circe-lagmon-mode)
  (add-hook 'circe-channel-mode-hook 'enable-lui-autopaste)

  (defun gna-irc ()
    (interactive)
    (circe "Libera")
    (circe "OFTC")))


(provide 'gna-chat)
