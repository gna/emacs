;;; -*- lexical-binding: t; -*-

(defconst gna-web-dir (expand-file-name "~/Documents/web.org/"))


(use-package org-static-blog
  :ensure t
  :custom
  (org-static-blog-publish-title "Guillermo Navarro-Arribas")
  (org-static-blog-publish-url "https://deic.uab.cat/~gnavarro/")

  (org-static-blog-publish-directory (expand-file-name "output/" gna-web-dir))
  (org-static-blog-posts-directory (expand-file-name "posts/" gna-web-dir))
  (org-static-blog-drafts-directory (expand-file-name "drafts/" gna-web-dir))

  (org-static-blog-enable-tags t)
  (org-static-blog-rss-excluded-tag "page")
  (org-static-blog-index-file "blog.html")
  (org-export-with-toc nil)
  (org-export-with-section-numbers nil)

  :init
  (defun gna-blog-read-template (filename)
    (gna-string-from-file (concat gna-web-dir "templates/" filename)))
  
  (defun gna-web-load-templates ()
    (interactive)
    (setq org-static-blog-page-header
          (gna-blog-read-template "page-header.html"))
    (setq org-static-blog-page-preamble
          (gna-blog-read-template "page-preamble.html"))
    (setq org-static-blog-page-postamble
          (gna-blog-read-template "page-postamble.html"))
    (setq org-static-blog-post-comments
          (gna-blog-read-template "post-comments.html")))
  :config
  (gna-web-load-templates))


(defun gna-web-publish-static-content (&optional buff-out)
  "Copy static content to org-static-blog-publish-directory."
  (interactive)
  (let ((static-dirs
         `(,(expand-file-name "static" gna-web-dir)
           ,(expand-file-name "files" org-static-blog-posts-directory)
           ,(expand-file-name "img" org-static-blog-posts-directory))))
    (dolist (f static-dirs)
      (gna-rsync f org-static-blog-publish-directory buff-out)))
  (message "Static content copied."))


;;; bib generation
(defconst gna-web-sbh-command "~/prj/simple-bibtex-html/sbh.py")
(defconst gna-web-remote "gnavarro@deic-home:public_html/")
(defconst gna-web-bib-files-dir "~/Documents/personal/cv/main-cv/bib/")
(defconst gna-web-bib-papers-dir
  (expand-file-name "papers" gna-web-bib-files-dir))
(defvar gna-web-bib-files
  (directory-files gna-web-bib-files-dir t "\\.bib$"))


(defun gna-web-sync-papers-dir (&optional buff-out)
  (gna-rsync gna-web-bib-papers-dir
             (expand-file-name "files" org-static-blog-posts-directory)
             buff-out)
  (message "Paper files copied."))


(defun gna-web-genbib (&optional buff-out)
  "Generate bibliography by calling sbh on gna-web-bib-files."
  (interactive)
  (let* ((outdir (expand-file-name "files" org-static-blog-posts-directory))
         (cmd (concat
               gna-web-sbh-command
               " --output-bib " (expand-file-name "fullbib.bib" outdir)
               " --output-html " (expand-file-name "fullbib.html" outdir)
               " " (string-join gna-web-bib-files " "))))
    (call-process-shell-command cmd nil buff-out nil)
    (message "bib generated.")))


;;; publish and deploy 
(defun gna-web-full-publish (&optional buff-out)
  (interactive "P")
  (gna-web-sync-papers-dir buff-out)
  (gna-web-genbib buff-out)
  (org-static-blog-publish 1)
  (gna-web-publish-static-content))


(defun gna-web-upload (&optional buff-out)
  (interactive)
  (gna-rsync org-static-blog-publish-directory gna-web-remote buff-out)
  (message "Uploaded"))


(defun gna-web-deploy ()
  (interactive)
  (let ((tmp-buff "*web-deploy*"))
    (with-output-to-temp-buffer tmp-buff
      (gna-web-full-publish tmp-buff)
      (gna-web-upload tmp-buff)
      (pop-to-buffer tmp-buff))))


(provide 'gna-org-static-blog)
