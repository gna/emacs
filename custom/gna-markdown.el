;;; -*- lexical-binding: t; -*-

(use-package markdown-mode
  :ensure t
  :init (dolist (ext '("\\.md$" "\\.markdown$"))
          (add-to-list 'auto-mode-alist (cons ext 'markdown-mode))))


(use-package markdown-toc
  :ensure t)


(provide 'gna-markdown)
