;;; -*- lexical-binding: t; -*-

(use-package web-mode
  :ensure t
  :mode (("\\.html\\'" . web-mode)
         ("\\.djhtml\\'" . web-mode)
         ("\\.jinja2\\'" . web-mode)
         ("\\.mako\\'" . web-mode)
         ("\\.php\\'" . web-mode))
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-attr-indent-offset 2)

  :config
  (setq web-mode-engines-alist
        '(("php" . "\\.phtml\\'")
          ("django" . "\\.jinja2\\'"))))

(provide 'gna-prog-web)
