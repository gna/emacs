;;; gna-org-denote.el ---                            -*- lexical-binding: t; -*-

;;; Code:


(use-package denote
  :ensure t
  :after (org)
  :bind
  (("C-c n n" . 'denote)
   :map org-mode-map
   ("C-c n i" . 'denote-link)
   ("C-c n I" . 'denote-link-add-links)
   ("C-c n b" . 'denote-link-backlinks)
   ("C-c n f" . 'denote-rename-file-using-front-matter)
   ("C-c n r" . 'denote-rename-file))
  :custom
  (denote-directory gna-notes-dir)
  (denote-date-prompt-use-org-read-date t)
  (denote-prompts '(subdirectory title keywords))
  (denote-known-keywords nil)
  (denote-rename-buffer-mode 1)
  :config
  (require 'denote-org-extras)
  (setq denote-org-front-matter
        "#+title:      %s
#+date:       %s
#+filetags:   %s
#+identifier: %s
#+Time-stamp: <>
\n")

;;; remove accents in filenames
  ;; TODO sluffigy filenames to replace accented chars by non-accented ones
  ;; See
  ;; - https://protesilaos.com/emacs/denote#h:d1e4eb5b-e7f2-4a3b-9243-e1c653817a4a
  ;; - https://emacs.stackexchange.com/a/78241
  

;;; meetings
  ;; (https://protesilaos.com/codelog/2024-09-20-emacs-use-denote-for-meetings-events/)
  
  (defvar gna-denote-colleagues gna-priv-denote-colleagues
    "List of names I collaborate with.
There is at least one file in the variable `denote-directory' that has
the name of this person.")

  (defvar gna-denote-colleagues-prompt-history nil
    "Minibuffer history for `gna-denote-new-meeting'.")

  (defun gna-denote-colleagues-prompt ()
    "Prompt with completion for a name among `gna-denote-colleagues'.
Use the last input as the default value."
    (let ((default-value (car gna-denote-colleagues-prompt-history)))
      (completing-read
       (format-prompt "New meeting with COLLEAGUE" default-value)
       gna-denote-colleagues
       nil :require-match nil
       'gna-denote-colleagues-prompt-history
       default-value)))

  (defun gna-denote-colleagues-get-file (name)
    "Find file in variable `denote-directory' for NAME colleague.
If there are more than one files, prompt with completion for one among
them.

NAME is one among `gna-denote-colleagues'."
    (if-let ((files (denote-directory-files (format "%s.*_meeting" name)))
             (length-of-files (length files)))
        (cond
         ((= length-of-files 1)
          (car files))
         ((> length-of-files 1)
          (completing-read "Select a file: " files nil :require-match)))
      (user-error "No files for colleague with name `%s'" name)))

  (defun gna-denote-new-meeting ()
    "Prompt for the name of a colleague and insert a timestamped heading therein.
The name of a colleague corresponds to at least one file in the variable
`denote-directory'.  In case there are multiple files, prompt to choose
one among them and operate therein.

Names are defined in `gna-denote-colleagues'."
    (declare (interactive-only t))
    (interactive)
    (let* ((name (gna-denote-colleagues-prompt))
           (file (gna-denote-colleagues-get-file name))
           (time (format-time-string "%F %a %R"))) ; remove %R if you do not want the time
      (with-current-buffer (find-file file)
        (goto-char (point-max))
        (insert (format "* [%s]\n\n" time))))))


(use-package consult-denote
  :ensure t
  :bind (("C-c n s" . 'consult-denote-find)
         ("C-c n g" . 'consult-denote-grep))
  :config
  (consult-denote-mode 1))





(provide 'gna-org-denote)
;;; gna-org-denote.el ends here
