;;; -*- lexical-binding: t; -*-
;; Some Elisp utility functions
;; named gna-0 so it is the first file loaded

;;; Code:

(defun gna-create-directory-if-not-exists (directory)
  "Create DIRECTORY if it does not exist."
  (unless (file-directory-p directory)
    (make-directory directory)
    (message "Directory %s created")))


(defun gna-string-from-file (file-path)
  "Return the contents of a file as a string"
  (with-temp-buffer
    (insert-file-contents file-path)
    (buffer-string)))


(defun gna-rsync (src dst &optional buff-out)
  "Run rsync with basic standard options.
Executes a syncrhonous rsync from SRC to DST. Optional argument
BUFF-OUT: if present generate output to BUFF-OUT (usually a temp
buffer)"
  (if buff-out
      (with-current-buffer buff-out
        (goto-char (point-max))
        (insert (format "\n* rsync %s -> %s: \n" src dst))))
  (let ((cmd (string-join
              (list "/usr/bin/rsync -avL --exclude=\".*\" --delete-excluded"
                    src dst) " ")))
    (call-process-shell-command cmd nil buff-out nil)))


(defun gna-private-get-credentials (server)
  "Get credentials from auth-source as (<username> <password>)."
  (let ((match (car (auth-source-search :host server :max 1))))
    (if match
        (let ((secret (plist-get match :secret))
              (user (plist-get match :user)))
          (list user (when secret (funcall secret)))))))


(defun gna-private-get-user (server)
  (car (gna-private-get-credentials server)))


(defun gna-private-get-secret (server)
  (cadr (gna-private-get-credentials server)))


;;; themes
(defun gna-disable-themes-all ()
  (interactive)
  (mapc #'disable-theme custom-enabled-themes))


(defun gna-load-theme (theme)
  (interactive)
  (gna-disable-themes-all)
  (load-theme theme t))


(defun xah-open-in-external-app (&optional @fname)
  "Open the current file or dired marked files in external app.
When called in emacs lisp, if @fname is given, open that.
URL `http://xahlee.info/emacs/emacs/emacs_dired_open_file_in_ext_apps.html'
Version 2019-11-04 2021-02-16"
  (interactive)
  (let*
      (($file-list
        (if @fname
            (progn (list @fname))
          (if (string-equal major-mode "dired-mode")
              (dired-get-marked-files)
            (list (buffer-file-name)))))
       ($do-it-p (if (<= (length $file-list) 5)
                     t
                   (y-or-n-p "Open more than 5 files? "))))
    (when $do-it-p
      (cond
       ((string-equal system-type "windows-nt")
        (mapc
         (lambda ($fpath)
           (shell-command
            (concat "PowerShell -Command \"Invoke-Item -LiteralPath\" "
                    "'" (shell-quote-argument (expand-file-name $fpath )) "'")))
         $file-list))
       ((string-equal system-type "darwin")
        (mapc
         (lambda ($fpath)
           (shell-command
            (concat "open " (shell-quote-argument $fpath))))  $file-list))
       ((string-equal system-type "gnu/linux")
        (mapc
         (lambda ($fpath)
           (let ((process-connection-type nil))
             (start-process "" nil "xdg-open" $fpath))) $file-list))))))


(defun gna-open-in-nautilus ()
  "Open directory of current file, or directory if in dired-mode,
with Nautilus (gnome files)."
  (interactive)
  (let ((path (if (string-equal major-mode "dired-mode")
                  (dired-current-directory)
                (buffer-file-name)))
        (process-connection-type nil))
    (start-process "" nil "/usr/bin/nautilus" path)))


(provide 'gna-0-elisp-util)

;;; gna-elisp-util.el ends here
