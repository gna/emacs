;;; -*- lexical-binding: t; -*-
;; clojure config

(use-package clojure-mode
  :ensure t)

(use-package cider
  :ensure t
  :commands cider-mode
  :config 
  (add-hook 'cider-mode-hook #'eldoc-mode))

(provide 'gna-prog-clojure)
