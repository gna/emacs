;;; -*- lexical-binding: t; -*-

;; writing / language

;;; flyspell
;; https://github.com/d12frosted/flyspell-correct
(use-package flyspell-correct
  :ensure t
  :after flyspell
  :bind (:map flyspell-mode-map ("C-;" . flyspell-correct-wrapper)))


;; https://github.com/PillFall/Emacs-LanguageTool.el
(use-package languagetool
  :ensure t
  :defer
  :commands (languagetool-check
             languagetool-clear-suggestions
             languagetool-correct-at-point
             languagetool-correct-buffer
             languagetool-set-language
             languagetool-server-mode
             languagetool-server-start
             languagetool-server-stop)
  :custom
  (languagetool-console-command
   "~/applications/LanguageTool/languagetool-commandline.jar")
  (languagetool-server-command
   "~/applications/LanguageTool/languagetool-server.jar")
  (languagetool-java-arguments '("-Dfile.encoding=UTF-8")))


(use-package flymake-languagetool
  :ensure t
  :hook ((text-mode       . flymake-languagetool-load)
         (latex-mode      . flymake-languagetool-load)
         (org-mode        . flymake-languagetool-load)
         (markdown-mode   . flymake-languagetool-load))
  :init
  (setq flymake-languagetool-server-jar
        "~/applications/LanguageTool/languagetool-server.jar")
  (setq flymake-languagetool-check-spelling t))


;; https://codeberg.org/martianh/lingva.el
(use-package lingva :ensure t)


;; reverso https://github.com/SqrtMinusOne/reverso.el
(use-package reverso
  :ensure t
  :config
  (setq reverso-languages '(english spanish)))


;; https://github.com/abo-abo/define-word
;; define-word, define-word-at-point, defaults to wordnik.com
(use-package define-word :ensure t)


;; https://github.com/minad/jinx
(use-package jinx
  :ensure t
  :hook (text-mode . jinx-mode)
  :bind (("M-$" . jinx-correct)
         ("C-M-$" . jinx-languages)))


(provide 'gna-writing)
