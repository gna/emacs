;;; -*- lexical-binding: t; -*-

(use-package go-mode
  :ensure t
  :config
  (setq gofmt-command "goimports")
  (add-hook 'go-mode-hook (lambda () (setq tab-width 4)))
  (add-hook 'before-save-hook 'gofmt-before-save))

(use-package go-eldoc
  :ensure t
  :config
  (add-hook 'go-mode-hook 'go-eldoc-setup))

(use-package go-guru
  :ensure t
  :config
  (setq go-guru-command "/usr/bin/golang-guru")
  (add-hook 'go-mode-hook #'go-guru-hl-identifier-mode))
  
(provide 'gna-prog-go)
