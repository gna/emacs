;; -*- lexical-binding: t; -*-

;;; builtin completion

(setq tab-always-indent 'complete)
(setq completion-auto-help 'always)
(setq completion-auto-help 'second-tab)
(setq completion-cycle-threshold nil)
(setq completion-ignored-extensions nil)
(setq completion-ignore-case t)
(setq completion-category-defaults nil)
(setq completion-category-overrides '((file (styles basic partial-completion))))

;; only show commands that apply to the current mode in M-x
(setq read-extended-command-predicate
      #'command-completion-default-include-p)


;;; orderless
;; https://github.com/oantolin/orderless
(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (orderless-matching-styles
   '(orderless-initialism orderless-literal orderless-regexp))
  (orderless-component-separator 'orderless-escapable-split-on-space))


;;; vertico
(use-package vertico
  :ensure t
  :custom
  (vertico-count 15)
  :config
  (vertico-mode))


;; ;; https://raw.githubusercontent.com/minad/vertico/main/extensions/vertico-directory.el
(use-package vertico-directory
  :after vertico
  :ensure nil
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))


;;; marginalia
;; https://github.com/minad/marginalia
;; add info to completions 
(use-package marginalia
  :ensure t
  :bind (:map minibuffer-local-map ("C-M-a" . marginalia-cycle))

  :custom
  (marginalia-annotators
   '(marginalia-annotators-heavy marginalia-annotators-light))

  :init
  (marginalia-mode))


;;; consult 
;; https://github.com/minad/consult
;; completion commands
(use-package consult
  :ensure t
  :bind (("C-c M-x" . consult-mode-command)
         ("C-c h" . consult-history)

         ("C-x r b" . consult-bookmark)
         ("C-x b" . consult-buffer)
         ("C-x p b" . consult-project-buffer)
         
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)
         ("M-g g" . consult-goto-line)
         ("M-g M-g" . consult-goto-line)
         ("M-g h" . consult-history)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ("M-g m" . consult-mark)
         ("M-g M" . consult-global-mark)
         ("M-g o" . consult-outline)
         
         ("<help> I" . consult-info)
         ("<help> M" . consult-man)
         
         ("M-y" . consult-yank-pop)

         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s c" . consult-locate)
         ("M-s g" . consult-ripgrep)
         
         :map isearch-mode-map
         ("M-s s" . consult-isearch-history)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         
         :map minibuffer-local-map
         ("M-s" . consult-history)
         ("M-r" . consult-history))
  
  :config
  (add-to-list 'consult-preview-excluded-files "\.gpg$")
  (consult-customize consult-bookmark
                     consult--source-bookmark :preview-key "M-."))


;; Jao's consult-recoll (https://codeberg.org/jao/consult-recoll)
(use-package consult-recoll
  :ensure t
  :after (transient embark)
  :bind (("M-s R" . #'consult-recoll)
         ("M-s r" . #'gna-transient-consult-recoll))
  :custom
  (consult-recoll-search-flags
   `("-c" ,(expand-file-name "~/var/recoll/recoll-documents")
     "-i" ,(expand-file-name "~/var/recoll/recoll-mail/xapiandb-mail")
     "-i" ,(expand-file-name
            "~/var/recoll/recoll-documents-personal/xapiandb-documents-personal")))

  :config
  (consult-recoll-embark-setup)

  (defun gna-transient-do-consult-recoll (args)
    (interactive (list (transient-args 'gna-transient-consult-recoll)))
    (consult-recoll (string-join args " ")))

  (transient-define-prefix gna-transient-consult-recoll ()
    [["Location"
      ("d" "Documents" "dir:~/Documents ")
      ("p" "papers" "dir:~/Documents/RESEARCH/papers ")
      ("t" "teaching" "dir:~/Documents/DOCENCIA ")
      ("n" "notes" "dir:~/Documents/org/notes ")
      ("z" "Zotero papers"
       "dir:~/Documents/bibliography/Zotero/storage mime:application/pdf ")
      ("m" "mail" "mime:message ")] 
     ["Type"
      ("i" "image" "mime:image/* ")
      ("f" "pdf" "mime:application/pdf ")
      ("l" "latex" "mime:text/x-tex ")]]
    ["Command"
     ("s" "Search" gna-transient-do-consult-recoll)]))


;; https://github.com/karthink/consult-dir
(use-package consult-dir
  :ensure t
  :bind (("C-x C-d" . consult-dir)
         :map vertico-map
         (("C-x C-d" . consult-dir)
          ("C-x C-j" . consult-dir-jump-file))))


;;; embark 
;; https://github.com/oantolin/embark/
;; contextual actions
(use-package embark
  :after which-key
  :ensure t
  :demand t
  :bind (("C-c i" . embark-dwim)
         ("<BLAH-i>" . embark-act) ;; C-i
         ("C-h b" . embark-bindings))
  :custom
  (prefix-help-command #'embark-prefix-help-command)

  :config
  (define-key embark-url-map (kbd "x") #'browse-url-xdg-open))


(use-package embark-consult
  :ensure t
  :after (embark consult)
  :demand t
  :hook
  (embark-collect-mode . embark-consult-preview-minor-mode))


;;; tempel
(use-package tempel
  :ensure t
  :bind (("M-/" . tempel-complete)
         ("M-*" . tempel-insert)
         :map tempel-map
         ("RET" . tempel-done)
         ("C-p" . tempel-previous)
         ("C-n" . tempel-next)
         ("TAB" . tempel-next))
  :custom
  (tempel-path (expand-file-name "templates/*.eld" gna-emacs-config-dir)))


;;; corfu
(use-package corfu
  :ensure t
  :bind (:map corfu-map ("<escape>" . corfu-quit))
  :init
  (global-corfu-mode)
  :custom
  (corfu-auto t)
  (corfu-cycle t)
  (corfu-separator ?\s)
  (corfu-quit-no-match 'separator)
  :config
  (corfu-history-mode 1)
  (with-eval-after-load 'savehist 
    (add-to-list 'savehist-additional-variables 'corfu-history))
  ;; no corfu-auto in eshell
  (add-hook 'eshell-mode-hook
            (lambda ()
              (setq-local corfu-auto nil)
              (corfu-mode)))
  (defun corfu-send-shell (&rest _)
    "Send completion candidate when inside comint/eshell."
    (cond
     ((and (derived-mode-p 'eshell-mode) (fboundp 'eshell-send-input))
      (eshell-send-input))
     ((and (derived-mode-p 'comint-mode)  (fboundp 'comint-send-input))
      (comint-send-input))))

  (advice-add #'corfu-insert :after #'corfu-send-shell))


;; https://github.com/jdtsmith/kind-icon
(use-package kind-icon
  :ensure t
  :after corfu
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))


(use-package cape
  :ensure t
  :bind  (("C-o p" . completion-at-point)
          ("C-o d" . cape-dabbrev)
          ("C-o a" . cape-abbrev)
          ("C-o h" . cape-history)
          ("C-o f" . cape-file)
          ("C-o w" . cape-dict)
          ("C-o x" . cape-tex)
          ("C-o c" . gna-cape-codeium)
          ("C-o e" . gna-cape-eglot))
  :init
  (global-unset-key "\C-o")
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-history)
  ;; (add-to-list 'completion-at-point-functions #'cape-tex)
  :config
  ;; Silence the pcomplete capf, no errors or messages!
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-silent)

  ;; Ensure that pcomplete does not write to the buffer
  ;; and behaves as a pure `completion-at-point-function'.
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-purify)

  (defun gna-cape-codeium (&optional interactive)
    "Call codeium capf interactively"
    (interactive (list t))
    (when interactive
      (cape-interactive #'codeium-completion-at-point)))

  (defun gna-cape-eglot (&optional interactive)
    "Call eglot capf interactively"
    (interactive (list t))
    (when interactive
      (cape-interactive #'eglot-completion-at-point))))


;;; codeium

(use-package codeium
  :load-path (lambda () (expand-file-name "codeium.el" gna-local-lisp-dir))
  :config
  
  (setq use-dialog-box nil) ;; do not use popup boxes

  ;; get codeium status in the modeline
  (setq codeium-mode-line-enable
        (lambda (api)
          (not (memq api '(CancelRequest Heartbeat AcceptCompletion)))))
  (add-to-list 'mode-line-format '(:eval (car-safe codeium-mode-line)) t)

  ;; use M-x codeium-diagnose to see apis/fields that would be sent to the local language server
  (setq codeium-api-enabled
        (lambda (api)
          (memq api
                '(GetCompletions Heartbeat CancelRequest GetAuthToken
                                 RegisterUser auth-redirect AcceptCompletion))))

  ;; we recommend limiting the string sent to codeium for better performance
  (defun my-codeium/document/text ()
    (buffer-substring-no-properties
     (max (- (point) 3000) (point-min)) (min (+ (point) 1000) (point-max))))
  (defun my-codeium/document/cursor_offset ()
    (codeium-utf8-byte-length
     (buffer-substring-no-properties
      (max (- (point) 3000) (point-min)) (point))))
  (setq codeium/document/text 'my-codeium/document/text)
  (setq codeium/document/cursor_offset 'my-codeium/document/cursor_offset))



;;; Codeium & eglot completion
;; Based on https://github.com/Exafunction/codeium.el/issues/82#issuecomment-1932467412

(defvar gna-codeium-enabled t)
(defvar gna-codeium-enabled-modes (list 'python-ts-mode))

(defalias 'gna-capf-codeium-eglot
  (cape-capf-super
   (cape-capf-silent
    (when (bound-and-true-p gna-codeium-enabled)
      (cape-capf-properties #'codeium-completion-at-point)))
   (cape-capf-buster #'eglot-completion-at-point)))

(defun gna-cape-codeium-eglot (&optional interactive)
  (interactive (list t))
  (when interactive
    (cape-interactive #'gna-capf-codeium-eglot)))

(defun gna-eglot-capf-config ()
  (if (member major-mode gna-codeium-enabled-modes)
      (setq-local completion-at-point-functions
                  (list #'gna-capf-codeium-eglot #'cape-file))))

(add-hook 'eglot-managed-mode-hook #'gna-eglot-capf-config)




(provide 'gna-completion)
