;;; -*- lexical-binding: t; -*-
;;; dired
;;; -guille-


(use-package dired
  :ensure nil
  :bind (:map dired-mode-map
              ("C-c C-r" . wdired-change-to-wdired-mode)
              ("C-c o" . dired-omit-mode)
              ("C-c C-x a" . org-attach-dired-to-subtree)
              ("?" . gna-dired-get-size))
  :custom
  (dired-listing-switches
   "-alFhDv --group-directories-first --time-style=long-iso")
  ;; guess target directory 
  (dired-dwim-target t)
  ;; delete files to trash
  (delete-by-moving-to-trash t)
  ;; vc-rename
  (dired-vc-rename-file t)
  ;; wdired
  (wdired-allow-to-change-permissions t)
  ;; hide all dot files except "." and ".." in dired-omit-mode (dired-x)
  (dired-omit-files "^\\...+$")

  ;;image-dired (bigger thumbs)
  (image-dired-thumb-size 256)
  (image-dired-thumb-width 256)
  (image-dired-thumb-height 256)

  :config
  ;; add dired directories to recentf
  (add-hook 'dired-mode-hook
            (lambda () (when recentf-mode
                         (recentf-add-file default-directory))))

  (defun gna-dired-get-size ()
    "Get size of files or dirs.
From emacswiki https://www.emacswiki.org/emacs/DiredGetFileSize)"
    (interactive)
    (let ((files (dired-get-marked-files)))
      (with-temp-buffer
        (apply 'call-process "/usr/bin/du" nil t nil "-sch" files)
        (message "Size: %s"
                 (progn 
                   (re-search-backward "\\(^[0-9.,]+[A-Za-z]+\\).*total$")
                   (match-string 1))))))

  ;; load dired-x
  (require 'dired-x)

  ;; shell guessing (dired-x)
  (setq dired-guess-shell-alist-user
        '(("\\.odt\\'\\|\\.ods\\'\\|\\.odp\\'\\|\\.odg\\'" "libreoffice")
          ("\\.docx\\'\\|\\.doc\\'" "libreoffice")
          ("\\.xlsx\\'\\|\\.xls\\'" "libreoffice")
          ("\\.pptx\\'\\|\\.ppt\\'" "libreoffice")
          ("\\.drawio\\'" "drawio")
          (".*" "setsid -w xdg-open"))))

;; dired-narrow (from https://github.com/Fuco1/dired-hacks)
(use-package dired-narrow
  :after dired
  :ensure t
  :bind (:map dired-mode-map
              ("/" . dired-narrow)))

(use-package dired-preview
  :ensure t
  :bind (:map dired-mode-map ("P" . dired-preview-mode))
  :custom
  (dired-preview-delay 0)
  (dired-preview-max-size (expt 2 20))
  (dired-preview-ignored-extensions-regexp
   (concat "\\."
           "\\(gz\\|"
           "zst\\|"
           "tar\\|"
           "xz\\|"
           "rar\\|"
           "zip\\|"
           "iso\\|"
           "epub"
           "\\)")))

;; https://github.com/purcell/diredfl
(use-package diredfl
  :ensure t
  :config
  (diredfl-global-mode))

;; https://github.com/clemera/dired-git-info
(use-package dired-git-info
  :ensure t
  :bind (:map dired-mode-map (")" . dired-git-info-mode)))

(use-package dired-rsync
  :ensure t
  :after dired
  :bind (:map dired-mode-map ("r" . dired-rsync))
  :config
  (add-to-list 'mode-line-misc-info
               '(:eval dired-rsync-modeline-status 'append)))


(use-package ready-player
  :ensure t
  :config
  (ready-player-mode +1))


;; https://github.com/shingo256/trashed
(use-package trashed :ensure t)


(defun gna-rename-file-with-timestamp ()
  "Rename the file at point by adding a timestamp of the form
\"%Y%m%d--\" at the begining of the filename."
  (interactive)
  (let ((file (dired-get-filename 'no-dir))
        (timestamp (format-time-string "%Y%m%d")))
    (rename-file file (concat timestamp "--" file) 1)
    (revert-buffer)))

;; ediff two marked files
;; https://oremacs.com/2017/03/18/dired-ediff/
(defun ora-ediff-files ()
  (interactive)
  (let ((files (dired-get-marked-files))
        (wnd (current-window-configuration)))
    (if (<= (length files) 2)
        (let ((file1 (car files))
              (file2 (if (cdr files)
                         (cadr files)
                       (read-file-name
                        "file: "
                        (dired-dwim-target-directory)))))
          (if (file-newer-than-file-p file1 file2)
              (ediff-files file2 file1)
            (ediff-files file1 file2))
          (add-hook 'ediff-after-quit-hook-internal
                    (lambda ()
                      (setq ediff-after-quit-hook-internal nil)
                      (set-window-configuration wnd))))
      (error "no more than 2 files should be marked"))))
(eval-after-load "dired"
  '(progn
     (define-key dired-mode-map (kbd "e") 'ora-ediff-files)))


(provide 'gna-dired)
