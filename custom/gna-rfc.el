;;; -*- lexical-binding: t; -*-

(use-package rfc-mode
  :ensure t
  :custom
  (rfc-mode-directory (expand-file-name "~/Documents/rfc/")))

(provide 'gna-rfc)
