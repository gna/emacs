;;; -*- lexical-binding: t; -*-

(use-package empv
  :ensure t
  :demand
  :after (consult embark transient)
  :config

  (let ((radio-file
         (expand-file-name "empv-radio-channels.eld" gna-emacs-config-dir)))
    (when (file-exists-p radio-file)
      (setq empv-radio-channels (with-temp-buffer
                                  (insert-file-contents radio-file)
                                  (goto-char (point-min))
                                  (read (current-buffer))))))
  
  (empv-embark-initialize-extra-actions)
  
  (transient-define-prefix gna-transient-empv ()
    [[("t" "toggle" empv-toggle)
      ("r" "radio" empv-play-radio)
      ("y" "youtube" empv-youtube)
      ("m" "music" empv-play-audio)
      ("x" "exit" empv-exit)]
     [("i" "current" empv-display-current :transient t)
      ("l"  "log" empv-log-current-radio-song-name)]
     [("<up>" "volume up" empv-volume-up :transient t)
      ("<down>" "volume down" empv-volume-down :transient t)]
     [("P" "playlist" empv-playlist-select)
      ("n" "playlist next" empv-playlist-next)
      ("p" "playlist prev" empv-playlist-prev)
      ("C" "playlist clear" empv-playlist-clear)]
     ])
  (global-set-key (kbd "C-x m") #'gna-transient-empv))


(provide 'gna-media)
;;; gna-media.el ends here
