;;; -*- lexical-binding: t; -*-
;;; gna-outline.el --- outline-mode

(require 'outline)

(setq outline-minor-mode-highlight 'append)
(setq outline-minor-mode-cycle t)

(let ((map outline-minor-mode-map))
  (define-key map (kbd "C-c C-n") 'outline-next-visible-heading)
  (define-key map (kbd "C-c C-p") 'outline-previous-visible-heading))

(defun gna-comment-only-outline ()
  (setq-local outline-regexp (format "[%s]\\{3,\\} " comment-start)))

(defun gna-eshell-prompt-outline ()
  (setq-local outline-regexp eshell-prompt-regexp))

(add-hook 'emacs-lisp-mode-hook #'gna-comment-only-outline)
(add-hook 'emacs-lisp-mode-hook #'outline-minor-mode)
(add-hook 'eshell-mode-hook #'gna-eshell-prompt-outline)
(add-hook 'eshell-mode-hook #'outline-minor-mode)
(add-hook 'LaTeX-mode-hook #'outline-minor-mode)

(provide 'gna-outline)
;;; gna-outline.el ends here
