;; -*- lexical-binding: t; -*-

;;; set local directories
(defconst gna-emacs-config-dir
  (file-name-directory (file-truename load-file-name))
  "Main Emacs configuration directory (where this file is located)")

(defconst gna-custom-dir (expand-file-name "custom/" gna-emacs-config-dir))
(defconst gna-local-lisp-dir
  (expand-file-name "site-lisp/" gna-emacs-config-dir))

(add-to-list 'load-path gna-local-lisp-dir)
(add-to-list 'load-path gna-custom-dir)

;;;; custom dirs
(defconst gna-docs-dir (expand-file-name "~/Documents/"))
(defconst gna-org-dir (expand-file-name "org/" gna-docs-dir))
(defconst gna-notes-dir (expand-file-name "notes/" gna-org-dir))
(defconst gna-bib-notes-dir (expand-file-name "bib/" gna-notes-dir))

(defconst gna-bib-dir (expand-file-name "bibliography/" gna-docs-dir))
(defconst gna-bib-pdf-dir (expand-file-name "files/" gna-bib-dir))

;;; create mising directories if needed
(dolist (dir (list gna-docs-dir gna-org-dir gna-notes-dir gna-bib-notes-dir
                   gna-bib-dir gna-bib-pdf-dir))
  (unless (file-directory-p dir)
    (make-directory dir)
    (message "Dir %s did not exist. Created." dir)))

;;; bib files from zotero if zotero-bibtex exists, otherwise nil
(defvar gna-bib-zotero-files
  (if (file-directory-p (concat gna-bib-dir "/zotero-bibtex/"))
      (directory-files
       (concat gna-bib-dir "/zotero-bibtex/") t "\\.bib$" nil)
    nil))

;;; packages
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("gnu-devel" . "https://elpa.gnu.org/devel/") t)
(add-to-list 'package-archives
             '("nongnu-devel" . "https://elpa.nongnu.org/nongnu-devel/") t)
(setq package-archive-priorities '(("gnu" . 2)
				   ("nongnu" . 1)))
(package-initialize)

(require 'use-package)

;; ELPA keyring
(use-package gnu-elpa-keyring-update :ensure t)

;; diminish 
(use-package diminish :ensure t)

;;; emacs' custom config's file
(setq custom-file (expand-file-name "emacs-custom.el" gna-emacs-config-dir))
;; (load custom-file)

;;; env

(setenv "EDITOR" "emacsclient")


;;;; direnv
(use-package direnv
  :ensure t
  :config
  (direnv-mode))


;;; midnight mode
(require 'midnight)
;; run it every 3 hours
(setq midnight-period 10800) ;; (eq (* 3 60 60) "3 hours")
;; clean-buffer-list: set expiration date of all buffers: 1 day (default 3 days)
(setq clean-buffer-list-delay-general 1)


;;; general configs 

;; use external converter for non supported image formats
(setq image-use-external-converter t)

;; show matching parents
(require 'paren)
(show-paren-mode t)


;; redefine C-g
;; from Protesilaos Stavrou
;; https://protesilaos.com/codelog/2024-11-28-basic-emacs-configuration/#h:1e468b2a-9bee-4571-8454-e3f5462d9321
(defun gna-keyboard-quit-dwim ()
  "Do-What-I-Mean behaviour for a general `keyboard-quit'.

The generic `keyboard-quit' does not do the expected thing when
the minibuffer is open.  Whereas we want it to close the
minibuffer, even without explicitly focusing it.

The DWIM behaviour of this command is as follows:

- When the region is active, disable it.
- When a minibuffer is open, but not focused, close the minibuffer.
- When the Completions buffer is selected, close it.
- In every other case use the regular `keyboard-quit'."
  (interactive)
  (cond
   ((region-active-p)
    (keyboard-quit))
   ((derived-mode-p 'completion-list-mode)
    (delete-completion-window))
   ((> (minibuffer-depth) 0)
    (abort-recursive-edit))
   (t
    (keyboard-quit))))

(define-key global-map (kbd "C-g") #'gna-keyboard-quit-dwim)


;;; default font
(set-face-attribute 'default nil :family "JetBrains Mono" :height 120)
;; (set-face-attribute 'default nil :family "Fira Code" :height 120) 
;; (set-face-attribute 'default nil :family "Hack" :height 120)
;; (set-face-attribute 'default nil :family "Inconsolata" :height 140)
;; (set-face-attribute 'default nil :family "Iosevka" :height 130)
;; (set-face-attribute 'default nil :family "Iosevka Comfy" :height 130)
;; (set-face-attribute 'default nil :family "DejaVu Sans Mono" :height 120)


;;; mode line
(line-number-mode 1)
(column-number-mode 1)


;;; fringe
(setq-default indicate-buffer-boundaries 'left)
(setq-default indicate-empty-lines +1)


;;; windows

;; ace-window
(use-package ace-window
  :ensure t
  :bind ("C-x o" . ace-window)
  :config
  ;; increase overlay face size
  (custom-set-faces
   '(aw-leading-char-face ((t (:height 3.0))))))


;; winner
(winner-mode 1)


;; activitites
(use-package activities
  :ensure t
  :bind
  (("C-x C-a C-n" . activities-new)
   ("C-x C-a C-d" . activities-define)
   ("C-x C-a C-a" . activities-resume)
   ("C-x C-a C-s" . activities-suspend)
   ("C-x C-a C-k" . activities-kill)
   ("C-x C-a RET" . activities-switch)
   ("C-x C-a b" . activities-switch-buffer)
   ("C-x C-a g" . activities-revert)
   ("C-x C-a l" . activities-list))
  :config
  (activities-mode)
  ;; Prevent `edebug' default bindings from interfering.
  (setq edebug-inhibit-emacs-lisp-mode-bindings t))


(defun gna-window-split-3v ()
  (interactive)
  (delete-other-windows)
  (split-window-right)
  (split-window-right)
  (balance-windows))

;; transient to resize/split windows
(with-eval-after-load "transient"
  (transient-define-prefix gna-transient-resize-window ()
    [["Horizontal"
      ("<left>" "-narrower-" shrink-window-horizontally :transient t)
      ("<right>" "-wider-" enlarge-window-horizontally :transient t)]
     ["Vertical"
      ("<down>" "|shorter|" shrink-window :transient t)
      ("<up>" "|longer|" enlarge-window :transient t)]
     ["Other"
      ("=" "equal" balance-windows)
      ("0" "delete current" delete-window)
      ("1" "delete other windows" delete-other-windows)]
     ["Split"
      ("3" "3 vertical" gna-window-split-3v)
      ("2" "2 vertical" split-window-right)
      ("h" "2 horizontal" split-window-below)]])
  (global-set-key (kbd "C-c #") #'gna-transient-resize-window))`


;;; cursor

(blink-cursor-mode -1)

(use-package pulsar
  :ensure t
  :custom
  (pulsar-delay 0.1)
  :config
  (setq pulsar-pulse-functions
	(append pulsar-pulse-functions
                '(ace-window
                  split-window-below
                  split-window-right
                  whole-line-or-region-kill-ring-save)))
  (setq pulsar-pulse-region-functions pulsar-pulse-region-common-functions)
  (pulsar-global-mode 1))

;;; icons everywhere (nerd icons)
;; recall to call nerd-icons-install-fonts to install the font families
(use-package nerd-icons :ensure t)

(use-package nerd-icons-completion
  :ensure t
  :after marginalia
  :config
  (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))

(use-package nerd-icons-corfu
  :ensure t
  :after corfu
  :config
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(use-package nerd-icons-dired
  :ensure t
  :hook
  (dired-mode . nerd-icons-dired-mode))


;;; use uniquify for buffer names
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)


;;; autorevert
(setq auto-revert-avoid-polling t)
(setq auto-revert-verbose nil)
(setq auto-revert-check-vc-info nil)
(require 'autorevert)
(global-auto-revert-mode 1)


;;; autoinsert
(require 'autoinsert)
(setq auto-insert t)
(setq auto-insert-query t)
(setq auto-insert-directory
      (expand-file-name "autoinsert" gna-emacs-config-dir))
(auto-insert-mode)


;;; history
;; recentf
(setq recentf-max-saved-items 1000)
(recentf-mode 1)

;; saveplace
(save-place-mode 1)

;; savehist
(setq savehist-additional-variables
      '(kill-ring search-ring regexp-search-ring))
(savehist-mode 1)


;;; proced
(use-package proced
  :custom
  (proced-enable-color-flag t)
  (proced-show-remote-processes t))


;;; casual
;; https://github.com/kickingvegas/casual
(use-package casual
  :ensure t
  :after (calc dired Info)
  :bind (
         :map calc-mode-map ("<BLAH-m>" . #'casual-calc-tmenu)
         :map dired-mode-map ("<BLAH-m>" . #'casual-dired-tmenu)
         :map Info-mode-map ("<BLAH-m>" . #'casual-info-tmenu)))


;;; printing
(require 'printing)
(setq pr-ps-printer-alist
      '((deic-bn "lpr" nil "-P " "deic-bn")
        (deic-color "lpr" nil "-P " "deic-color")))
;; (setq pr-txt-printer-alist
;;       '((deic-bn nil nil "-P " "deic-bn")
;;         (deic-color nil nil "-P " "deic-color")))

;;; custom packages
(defvar gna-custom-pkgs
  (mapcar (lambda (f) (intern (substring f 0 -3)))
          (directory-files gna-custom-dir nil "gna-.+\\.el$")))

;; load custom pkgs (function taken from jao)
(defun gna-load-custom-pkgs ()
  (interactive)
  (mapc #'(lambda (pkg)
            (message 
             (format "Loading %s... %s." 
                     pkg
                     (condition-case err
                         (if (require pkg nil t) "ok" "not found")
                       (error (error-message-string err))))))
        gna-custom-pkgs))


(gna-load-custom-pkgs)


