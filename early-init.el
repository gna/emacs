;; -*- lexical-binding: t; -*-
;;
;; emacs early-init


;;; frame
(setq frame-resize-pixelwise nil)
(setq frame-inhibit-implied-resize t)
(setq-default frame-title-format '("%@ %b [%m]"))

;; do not use dialog boxes  
(setq use-dialog-box nil)
(setq use-file-dialog nil)

(setq use-short-answers t)

;; disable graphical elements
(menu-bar-mode -1)
(tool-bar-mode -1)
(set-scroll-bar-mode nil)

(setq inhibit-splash-screen t)
(setq inhibit-startup-screen t)
(setq inhibit-startup-message t)

(setq visible-bell t)

;;; packages available at startup
(setq package-enable-at-startup t)

;; log native-comp warnings
(setq native-comp-async-report-warnings-errors 'silent)

;; C-i, C-m, C-[ keys
;; https://emacsnotes.wordpress.com/2022/09/11/three-bonus-keys-c-i-c-m-and-c-for-your-gui-emacs-all-with-zero-headache/
(add-hook
 'after-make-frame-functions
 (defun setup-blah-keys (frame)
   (with-selected-frame frame
     (when (display-graphic-p)
       (define-key input-decode-map (kbd "C-i") [BLAH-i])
       (define-key input-decode-map (kbd "C-[") [BLAH-lsb]) ; left square bracket
       (define-key input-decode-map (kbd "C-m") [BLAH-m])))))
